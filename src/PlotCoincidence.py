#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 14 03:17:47 2018

@author: jzuber
"""

import numpy as np
import matplotlib.pyplot as plt
import plotly.offline as py
import plotly.graph_objs as go
from matplotlib import rcParams

def plot_matplotlib(data, mutations, filename):
    rcParams['xtick.direction'] = 'out'
    rcParams['ytick.direction'] = 'out'
    
    x, y = np.meshgrid(range(1,len(mutations)+1), range(1,len(mutations)+1))
        
    plt.pcolormesh(x, y, data, cmap='Reds')
    plt.colorbar() 
    
    plt.savefig(filename)

def plot_plotly(data, mutations, counts, averages, filename):
    hover_text=[]
    for i in range(len(mutations)):
        row = []
        for j in range(len(mutations)):
            htext = mutations[i] + ':' + mutations[j] + '<br>'
            htext += 'Coincidence: '+str(data[i,j])
            row.append(htext)
        hover_text.append(row)
    
    trace1 = go.Heatmap(
        z=data,
        y=mutations,
        x=mutations,
        text=hover_text,
        hoverinfo='text',
        colorscale='Reds')
    
    trace2 = go.Bar(
        y=mutations,
        x=counts,
        name='Mutation Count',
        xaxis='x2',
        orientation='h')
        
    trace3 = go.Bar(
        y=averages,
        x=mutations,
        name='Average Coincidence',
        yaxis='y2'
    )
    
    trace4 = go.Scatter(
        y=averages,
        x=counts,
        xaxis='x2',
        yaxis='y2',
        mode = 'markers',
        text=mutations,
        hoverinfo='text'
    )
    
    
    # Combine the graph objects
    data = [trace1, trace2, trace3, trace4]
    
    # Define the graph layout
    layout = go.Layout(
        title='Mutation Coincidence Rates',
        showlegend=False,
        autosize=False,
        width=1500,
        height=1000,
        hovermode='closest',
        bargap=0,
    
        yaxis=dict(domain=[0, 0.8], linewidth=2, linecolor='#444', title='Given Mutation',
                   showgrid=False, zeroline=False, ticks='outside', showline=True, mirror=True),
    
        xaxis=dict(domain=[0, 0.8],linewidth=2,linecolor='#444', title='Coincident Mutation',
                   showgrid=False, zeroline=False, ticks='outside', showline=True, mirror=False),
                   
        yaxis2=dict(domain=[0.85, 1], linewidth=2, linecolor='#444', title='Average Coincidence',
                   showgrid=False, zeroline=False, ticks='outside', showline=True, mirror=True),
    
        xaxis2=dict(domain=[0.85, 1],linewidth=2,linecolor='#444', title='Mutation Count',
                   showgrid=False, zeroline=False, ticks='outside', showline=True, mirror=False))
        
    # Make the figure
    fig = go.Figure(data=data, layout=layout)
    
    # Export the html file
    url = py.plot(fig, filename=filename)

def plot_coincidence(ifile, ofile):
    ilines = ifile.readlines()
    ifile.close()
    mutations = ilines[0].strip().split(',')[2:]
    mutations = [i.strip() for i in mutations]
    
    averages = ilines[1].strip().split(',')[2:]
    averages = [i.strip() for i in averages]
    data=[]
    counts = []
    for line in ilines[2:]:
        line = line.split(',')
        counts.append(int(line[1]))
        line = [float(i) for i in line[2:-1]]
        data.append(line)
        
    ci = np.asarray(data)

    if ofile[-4:]=='html':
        plot_plotly(ci,mutations,counts,averages,ofile)
    else:
        plot_matplotlib(ci,mutations,ofile)

#ifile = open('../Output/Postsort/MutationCoincidence.csv')
#ofile = '../Output/CoincidenceMap.html'
#plot_coincidence(ifile, ofile)

ifile = open('../Output/YU2dSMs3_mutation_ci_3.tab')
ofile = '../Output/YU2dSMs3_mutation_ci_3.html'
plot_coincidence(ifile, ofile)

ifile = open('../Output/YU2-Prescreen_mutation_ci_3.tab')
ofile = '../Output/YU2-Prescreen_mutation_ci_3.html'
plot_coincidence(ifile, ofile)
#
#ifile = open('../Output/Prescreen_mutation_ci.tab')
#ofile = '../Output/Prescreen_mutation_ci.html'
#plot_coincidence(ifile, ofile)