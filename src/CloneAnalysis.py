# -*- coding: utf-8 -*-
"""
Created on Fri Jul 13 17:55:11 2018

@author: jzuber
"""
import os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio import pairwise2
from Bio.Alphabet import generic_dna, generic_protein
from math import ceil
#from yattag import Doc


def GetFiles(path):
    """GetFiles

    Get the names of the files that are in a specified path.  If the path does
    not exist or points to a file, an empty list is returned.

    Argurments:
        path {string}
            The path to the folder being queried
    
    Returns:
        folders {list[strings]}
            A list of file names that exist in the specified path
    """
    if os.path.exists(path):
        # Check if the path points to a directory
        if os.path.isdir(path):
            files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

            # Alphabetize the list of files
            files.sort()
        else:
            files = []
    else:
        files = []
    
    return files


def trim(string):
    N_pos = []
    for i in range(len(string)):
        if string[i]=='N':
            N_pos.append(i)
            
    first_pos = 0
    for i in range(len(N_pos)):
        if N_pos[i+1] - N_pos[i] > 25:
            first_pos = N_pos[i]+1
            break
    
    last_pos = 0
    for i in range(len(N_pos)):
        j = len(N_pos)-i-1
        if N_pos[j] - N_pos[j-1] > 25:
            last_pos = N_pos[j]
            break
    
    return string[first_pos:last_pos]
    
def score(ref, test, i):
    align_score = 0
    for j in range(len(test)):
        if test[j]==ref[i+j]:
            align_score+=1
        if i+j >= len(ref)-1:
            break
    
    return align_score
    
    
def align(ref, test):
    max_index = 0
    max_score = 0
    
    for i in range(len(ref)):
        ascore = score(ref,test,i)
        if ascore > max_score:
            max_index = i
            max_score = ascore
    
    return max_index, max_score


def get_mutations(ref,test,offset=0):
    i = 0
    mutations=[]
    for a,b in zip(ref,test):
        i+=1
        if a != b:
            mutations.append(a+str(alignment[offset+i])+b)
    return mutations


def get_codon_mutations(ref, sequences):
    codon_mutations = dict()
    for seq in sequences:
        mutations = []
        for i in range(len(ref)):
            a = ref[i]
            b = seq[i]
            if a!=b:
                codon_start = i - i%3
                old_codon = ref[codon_start:codon_start+3]
                new_codon = seq[codon_start:codon_start+3]
                mutations.append((codon_start, old_codon, new_codon, count_mutations(old_codon, new_codon)))
        codon_mutations[names[seq]]=mutations
    return codon_mutations


def write_mutations(mutations, file_path):
    ofile = open(file_path,'w')
    ofile.write('Sample\tMutations\n')
    for key in mutations.keys():
        ofile.write(key)
        ofile.write('\t')
        ofile.write(', '.join(mutations[key]))
        ofile.write('\n')
    ofile.close()

def write_nuc_mutations(sequences, reference, names, filename, offset=0):
    ofile = open(filename, 'w')
    ofile.write('Clone\tMutations\n')
    for seq in sequences.keys():
        ofile.write(names[seq])
        ofile.write('\t')
        oline = ''
        for i, (a,b) in enumerate(zip(sequences[seq]._data, reference._data)):
            if a != b:
                oline+=b+str(i+offset)+a+', '
        ofile.write(oline[:-2]+'\n')
    ofile.close()
        
    

def mutation_frequency(mutations):
    mutation_freq = dict()
#    num_clones = len(mutations.keys())
    for value in mutations.values():
        for item in value:
            if item not in mutation_freq.keys():
                mutation_freq[item]=1
            else:
                mutation_freq[item]+=1
            
#    for key in mutation_freq.keys():
#        mutation_freq[key]/=num_clones
#    
    return mutation_freq
        

def mutation_coincidence(mutations):
    mutation_ci = dict()
    for value in mutations.values():
        for i in range(len(value)):
            if value[i] not in mutation_ci.keys():
                mutation_ci[value[i]]=dict()
            for j in value[i:]:
                if j not in mutation_ci.keys():
                    mutation_ci[j]=dict()
                
                if j not in mutation_ci[value[i]]:
                    mutation_ci[value[i]][j]=1
                else:
                    mutation_ci[value[i]][j]+=1
                
                if value[i] not in mutation_ci[j]:
                    mutation_ci[j][value[i]]=1
                else:
                    if value[i] != j:
                        mutation_ci[j][value[i]]+=1
    return mutation_ci


def getKey(item):
    return '0'*(7-len(item))+item[1:]


def write_mutation_freq(mutation_freq, filename):
    ofile = open(filename,'w')
    all_mutations = [i for i in mutation_freq.keys()]
    all_mutations.sort(key=getKey)
    ofile.write('Mutation\tCount\n')
    for mut in all_mutations:
        ofile.write(mut)
        ofile.write('\t')
        ofile.write(str(mutation_freq[mut]))
        ofile.write('\n')
    ofile.close()


def write_mutation_ci(mutation_freq, mutation_ci, filename):
    ofile = open(filename,'w')
    all_mutations = [i for i in mutation_ci.keys()]
    all_mutations.sort(key=getKey)
    ofile.write(', '+','.join(all_mutations)+'\n')
    for mut1 in all_mutations:
        ofile.write(mut1+', ')
        for mut2 in all_mutations:
            try:
                ofile.write(str(mutation_ci[mut1][mut2]/mutation_freq[mut1])+', ')
            except:
                ofile.write('0, ')
        ofile.write('\n')
    ofile.close()
    
    
def write_aa_sequence(AA_seqs, names, filename):
    ofile = open(filename,'w')
    ofile.write('Clone\tAA Sequence\n')
    for sequence in AA_seqs.keys():
        ofile.write(names[sequence])
        ofile.write('\t')
        ofile.write(AA_seqs[sequence])
        ofile.write('\n')
    ofile.close()
    
def count_mutations(ref,seq):
    return sum([a!=b for a, b in zip(ref,seq)])
    
def get_alignment(filename,template_name, ref_name):
    ifile = open(filename)
    seqs = dict()
    for line in ifile:
        words = line.split()
        if len(words)==3:
            if words[0] in seqs.keys():
                seqs[words[0]]+=words[1]
            else:
                seqs[words[0]]=words[1]
    i=0
    j=0
    algn = dict()
    for a,b in zip(seqs[template_name],seqs[ref_name]):
        if a != '-':
            i+=1
        if b != '-':
            j+=1
        
        if i>0:
            algn[i]=j
            
    return algn

#def generate_html_report(ref,AA_seqs,filename):
#    #Print Sequence Table
#    line_width=120
#    sequences=[i for i in AA_seqs.keys()]
#    
#    doc, tag, text = Doc().tagtext()
#
#    with tag('html'):
#        with tag('body'):
#            for i in range(ceil(len(ref._data)/line_width)):
#                for seq in sequences:
#                    with tag('code'):
#                        text('some text')
#
#    result = doc.getvalue()
#    return result

def main(in_folder, out_folder):
    ref_file = os.path.join(in_folder,'ref.fasta')
    
    seq_files = GetFiles(in_folder)
    seq_files = [i for i in seq_files if i[-4:]=='.seq']
    sequences = []
    for seq_file in seq_files:
        input_file=os.path.join(in_folder, seq_file)
        seqs = SeqIO.parse(open(input_file),'fasta')
        for fasta in seqs:
            sequence = str(fasta.seq.reverse_complement())
            if sequence != "":
                sequences.append(trim(sequence))
    
    del sequence
    del seq_file
    del input_file
    
    for reference in SeqIO.parse(open(ref_file),'fasta'):
        break
    #for plasmid in SeqIO.parse(open(os.path.join(in_folder,'plasmid.fasta')),'fasta'):
    #    break
    #alignments = pairwise2.align.globalms(reference.seq, plasmid.seq, 3, -1, -100, 0)
    
    #alignments = pairwise2.align.globalms(reference.seq, sequences[0], 3, -1, -100, 0)
    
    offsets = []
    sequences2 = []
    
    for sequence in sequences:
        i, iscore = align(reference.seq, sequence)
        offsets.append(i)
        if i+len(sequence)> len(reference.seq)-1:
            sequences2.append(sequence[:len(reference.seq)-i])
    
    sequences3 = dict()
    names = dict()
    for sample, seq in zip(seq_files, sequences2):
        sample_name = sample[:sample.index('-')]
        seq = seq[-636:]
        
        if seq not in names.keys():
            names[seq]=sample_name
            sequences3[seq] = Seq(seq,generic_dna)
        else:
            names[seq]+=', '+sample_name
            
    #sequences3 = [Seq(i[-636:],generic_dna) for i in sequences2]
    ref = Seq(reference.seq._data[-636:],generic_dna).translate()._data
#    ref_seq = reference.seq._data[-636:]
    AA_seqs=dict()
    for sequence in sequences3.keys():
        alignments = pairwise2.align.globalms(reference.seq._data[-636:], sequences3[sequence]._data, 2, -1, -5, -1)
        
        if (alignments[0][1].find('-')==-1 and alignments[0][0].find('-')==-1):
            AA_seqs[sequence] = sequences3[sequence].translate()._data
        
    offset = 431
    global alignment
    alignment = get_alignment('../Input/Alignment.txt','YU2','HXB2') 
    
    mutations = dict()
    for sample in AA_seqs.keys():
        sample_name = names[sample]
        mutations[sample_name] = get_mutations(ref, AA_seqs[sample], offset)
    
    write_mutations(mutations,'../Output/Mutations_List.txt')
    write_nuc_mutations(sequences3, reference[-636:], names, '../Output/Nuc_Mutations_list.txt', len(reference)-636)
    mutation_freq=mutation_frequency(mutations)
    write_mutation_freq(mutation_freq,'../Output/MutationFrequency.txt')
    mutation_ci = mutation_coincidence(mutations)
    write_mutation_ci(mutation_freq,mutation_ci,'../Output/MutationCoincidence.csv')
    write_aa_sequence(AA_seqs, names, '../Output/AA_Sequences.txt')
    
    
in_folder = '../Input/Seq3/'
out_folder = '../Output/'
alignment = None

main(in_folder, out_folder)
    
#    index = (626-431-1)*3
#    codons = dict()
#    for seq in sequences3.values():
#        codon = seq._data[index:index+3]
#        if codon not in codons.keys()`:
#            codons[codon]=1
#        else:
#            codons[codon]+=1
#            
#    
#    ref_seq[index:index+3]
#
#vals = get_codon_mutations(reference.seq._data[-636:],[i._data for i in sequences3.values()])
#vals2=[]
#for line in vals.values():
#    vals2.extend(line)
#vals2=list(set(vals2))
#vals2.sort()