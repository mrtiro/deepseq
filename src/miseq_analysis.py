# -*- coding: utf-8 -*-
"""
This script is used to analyze miseq data for sequencing performed on randomly
mutagenized libraries.  This script will import sequencing reads, filter the 
reads, calculate mutation frequencies for each position, identify amino acid
substitutions, and calculate mutation coincidence rates (how frequently each 
mutations appears with other mutations).
"""
import sys
import pickle
try:
    from Bio import SeqIO
    from Bio.Seq import Seq
    from Bio import pairwise2
    from Bio.Alphabet import generic_dna
except ImportError:
    print('This script requires biopython to run')
    sys.exit()

from collections import Counter
from time import time

import numpy as np
import matplotlib.pyplot as plt

from statistics import mean, stdev

class STRUCT:
    """
    This class approximates a C++ struct to hold multiple variables in a single
    object.
    
    """
    def __init__(self, **kwds):
        """__init__
        Creates the struct.  The variables contained in the STRUCT are defined
        by the keyword arguments passed to the constructor.
        
        Example:
            Struct_obj = STRUCT(
                            variable1=value1,
                            variable2=value2,
                            )
        """
        self.__dict__.update(kwds)
        
        
    def update(self, **kwds):
        self.__dict__.update(kwds)

        
    def print_conf(self):
        """
        This function will print out the fields and values contained in a configuration object
        """
        # Get all the key lengths
        lengths = [len(key) for key in self.__dict__.keys()]

        # Determine the length of the longest key
        max_len = max(lengths)

        # Print each key
        for key, value in self.__dict__.items():
            print(key, ' '*(max_len-len(key))+':  ', value)     
            

class SeqAnalysis:
    """SeqAnalysis
    This class is used to run and store the results of sequencing analysis.  It
    also ensures the analyses are executed in the proper order.
    """
    def __init__(self, ref_seq=None):
        """__init__
        Initializes the class object using a reference sequence.  If no 
        reference sequence is given, it can be inferred from the consensus 
        sequence later.
        """
        self.insertions = dict()
        self.deletions = dict()
        self.numbers=dict()

        # If a reference sequence is provided
        if ref_seq:
            if ref_seq.strip("ACGT") == "":
                # The input string the the sequence itself
                self.ref_seq = ref_seq
            else:
                # The input string is treated as a filepath
                try:
                    sfile = open(ref_seq)
                    slines = sfile.readlines()
                    sfile.close()
                    
                    # Strip the whitespace from each line
                    slines = [line.strip() for line in slines]
                    
                    # If the sequence in the file spans multiple lines, join them into 
                    # a single string
                    self.ref_seq = "".join(slines)

                except FileNotFoundError:
                    print('The file \''+ref_seq+'\' cannot be opened.  Omitting the reference sequence.')
                    self.ref_seq = None
                    self.seq_length = None
                
            if self.ref_seq:
                # Store the sequence length
                self.seq_length = len(self.ref_seq)

                for i in range(self.seq_length+1):
                    self.insertions[i]=0
                    self.deletions[i]=0

        else:
            self.ref_seq = None
            self.seq_length = None
        
        # This flag indicated whether the sequences have been filtered to 
        # remove frameshifted and/or low quality sequences
        self.filtered = False
        
        self.seq_counts = None
        self.base_counts = None
        self.norm_base_count = None
        self.timer = dict()
        self.stack = []
        
        # Stores sequences that have been filtered out from the analysis
        self.bad_seqs = dict()   
        
       
    def get_sequence_counts(self, input_file, rev=False):
        """get_sequence_counts
        This function counts the number of occurances of each specific sequence
        in the fastq file.  The results are stored in the class attribute 
        self.seq_counts.
        
        Args:
            input_file {str}
                A path to the fastq file containing the joined reads
        """
        print('Importing sequences... ', end='')
        sys.stdout.flush()
        start = time()
        sequences = []
    
        fasta_sequences = SeqIO.parse(open(input_file),'fastq')
        
        for fasta in fasta_sequences:
            if rev:
                sequence = str(fasta.seq.reverse_complement())#process_seq(fasta)
            else:
                sequence = str(fasta.seq)#process_seq(fasta)
                
            if sequence != "":
                sequences.append(sequence)
        
        self.seq_counts = Counter(sequences)

        self.timer['Read Sequence Counts'] = time()-start
        
        print('Done')
        sys.stdout.flush()
        
        self.numbers['Total Reads']=sum(self.seq_counts.values())
        self.numbers['Total Unique Reads']=len(self.seq_counts)
        self.numbers["Total Sequences with multiple reads"]=len([i for i in self.seq_counts.values() if i > 1])


    def calc_base_frequency(self):
        """calc_base_frequency
        Calculates the frequency of each base at each position of the sequence.
        The results are stored in the class property self.base_counts
        """
        if not self.seq_counts:
            print('No sequence information found.  Have you imported it?')
            return
            
        if not self.filtered:
            self.filter_sequences()

        start = time()
            
        self.base_counts = [{'A':0,'T':0,'C':0,'G':0,'N':0} for i in range(self.seq_length)]
            
        # Get sequence counts
        for sequence in self.seq_counts.keys():
            count = self.seq_counts[sequence]
            for i in range(self.seq_length):
                self.base_counts[i][sequence[i]]+= count

        self.timer['Calculate Base Frequency'] = time()-start            
        

    def normalize_base_frequency(self):
        """normalize_base_frequency
        Normalizes the frequency of each base at each position of the sequence to
        get the probability of each base at each position.
        """
        
        if not self.base_counts:
            self.calc_base_frequency()
        
        start = time()
        
        self.norm_base_count=None

        if not self.norm_base_count:
            self.norm_base_counts = [{'A':0,'T':0,'C':0,'G':0} for i in range(self.seq_length)]
            self.mode_seq = ''
            for i in range(self.seq_length):
                total = sum(self.base_counts[i].values())
                max_nuc = dict_max(self.base_counts[i])
                
                for nuc in self.base_counts[i].keys():
                    if nuc != max_nuc and nuc != 'N':
                        self.norm_base_counts[i][nuc] = self.base_counts[i][nuc]/total
                
                self.mode_seq += max_nuc
                
            if not self.ref_seq:
                self.ref_seq = self.mode_seq
                for i in range(self.seq_length+1):
                    self.insertions[i]=0
                    self.deletions[i]=0

        self.timer['Normalize Base Frequency'] = time()-start            

        
    def filter_sequences(self,min_count=None):
        """filter_sequences
        Filters sequences that are the wrong length, have frameshifts, and are
        too uncommon and may be the result of sequencing read errors.
        
        Arguments:
            min_count {int=None}:
                A flag to specify whether sequences that only appear rarely 
                should be filtered out.  If the value is given, any sequence 
                that appears a fewer number of times than the value are 
                filtered
        """
        start = time()
        
        if self.ref_seq:
            # Only perform the filtering if it has not been done before
            if not self.filtered:
                # A list of the sequences to process
                seqs = [i for i in self.seq_counts.keys()]
                
                # Defines how frequently the progress bar updates
                increment = 0.05            
                
                # Tracks how many sequences have been processed
                count = 0
                
                # Defines the next time the progress bar will the updated
                threshold = increment
                
                # Start the progress bar
                print('|', end='')
                sys.stdout.flush()
                
                # Initialize trackers for insertions and deletions
                self.total_inserts = 0
                self.total_dels = 0
                
                # Process each sequence
                for sequence in seqs:
                    # Increment the sequence count
                    count += 1
                    
                    # Filter singleton sequence, if specified
                    if min_count:
                        if self.seq_counts[sequence]<min_count:
                            self.seq_counts.pop(sequence)
                        
                    # Check if the sequence needs to be analyzed for frameshifts
                    elif len(sequence) != self.seq_length or count_mutations(sequence,self.ref_seq) > 5:
                        # Generate the alignment                        
                        alignments = pairwise2.align.globalms(self.ref_seq, sequence, 2, -1, -5, -1)
                        
                        # If there are gaps in the alignment, remove the sequence
                        if not (alignments[0][1].find('-')==-1 and alignments[0][0].find('-')==-1):
                            # Move the sequence to bad_seqs                            
                            self.bad_seqs[sequence]=self.seq_counts.pop(sequence)
                            
#                            # Track the number of insertions and deletions and
#                            # where they occur
#                            inserts, dels = find_frameshifts(alignments[0])
#                            self.total_inserts += len(inserts)
#                            self.total_dels += len(dels)
#                            for i in inserts:
#                                try:
#                                    self.insertions[i]+=1
#                                except KeyError:
#                                    print(i)
#                            for i in dels:
#                                self.deletions[i]+=1
#                                    
#                            if len(inserts) + len(dels) == 0:
#                                self.stack.append(sequence)
                    
                    # Update the progress bar
                    if count/len(seqs) > threshold:
                        threshold += increment
                        print('#', end='')
                        sys.stdout.flush()
                
                # Finish the progress bar
                print('| Done')
                sys.stdout.flush()
                
                # Mark the filtering as having been done
                self.filtered = True
                
                # Track the time it took to perform the filtering
                self.timer['Filter Sequences'] = time()-start            

        else:
            # If there is not reference sequence, only filter based on sequence
            # length
        
            # Get a list of all the sequence lengths
            seq_lens = [len(i) for i in self.seq_counts.keys()]
            
            # Get the frequency of each length
            len_counts = Counter(seq_lens)
            
            # Find the most commmon sequence length
            self.seq_length = dict_max(len_counts)
            
            # Get a list of the sequences
            seqs = [i for i in self.seq_counts.keys()]
            
            # Iterate through the sequences and check the sequence length
            for sequence in seqs:
                if len(sequence) != self.seq_length:
                    # Move the sequence to bad_seqs
                    self.bad_seqs[sequence]=self.seq_counts.pop(sequence)
        
        self.numbers["Total Reads After Filter"]=sum(self.seq_counts.values())
        self.numbers['Total Unique Reads After Filter']=len(self.seq_counts)
        self.numbers["Total Sequences with multiple reads After Filter"]=len([i for i in self.seq_counts.values() if i > 1])
                        
            
    def filter_singletons(self):
        """filter_singletons
        Removes all sequences that appear only once in the sequencing, assuming
        that they are the result of sequencing read errors.
        """
        # Get a list of sequences
        seqs = [i for i in self.seq_counts.keys()]
        
        # If the sequence appears only once, move to bad_seqs        
        for seq in seqs:
            if self.seq_counts[seq]==1:
                self.bad_seqs[seq]=self.seq_counts.pop(seq)
        
        
    def write_base_frequency_table(self, output_file):
        """write_base_frequency_table
        Outputs a file containing the frequenciies at which each base mutates
        to each possible nucleotide.
        
        Arguments:
            output_file {str}:
                A path to the output file
        """
        # Open output file        
        ofile = open(output_file,'w')
        
        # Write header
        ofile.write('Consensus, A, C, G, T\n')
        
        # Write lines
        for i in range(self.seq_length):
            ofile.write(self.ref_seq[i]+', ')
            for nuc in ['A','C','G','T']:
                ofile.write(str(self.norm_base_counts[i][nuc])+', ')
            ofile.write('\n')
        
        # Close output file
        ofile.close()
        
        
    def calc_mutation_dist(self):
        """calc_mutation_dist
        Calculates the mutation count distribution
        """
        start = time()
        
        # If the sequences have not been filtered, filter them now
        if not self.filtered:
            self.filter_sequences()
        
        #Initialize the output
        mutation_counts=dict()
        
        # Iterate through the sequences
        for sequence in self.seq_counts.keys():
            # Count the mutations in the sequence
            num_mutations = count_mutations(sequence, self.ref_seq)
            
            # Add a count for that number of mutations
            if num_mutations not in mutation_counts.keys():
                mutation_counts[num_mutations] = self.seq_counts[sequence]
            else:
                mutation_counts[num_mutations] += self.seq_counts[sequence]
        
            # Add a count for that number of mutations
#            if num_mutations not in mutation_counts.keys():
#                mutation_counts[num_mutations] = 1
#            else:
#                mutation_counts[num_mutations] += 1

        self.timer['Calculate Mutation Distribution'] = time()-start            

        return mutation_counts
        
    
    def calc_substitution_rates(self):
        """calc_substitution_rates
        Calculates the non-position-specific substitution rates (i.e. the rate
        of A->C substitutions).  
        
        Returns:
            mut_rates {dict{str:float}
        """
        start = time()
        self.mut_rates = dict()
        for nuc1 in ['A','C','G','T']:
            for nuc2 in ['A','C','G','T']:
                if nuc1 != nuc2:
                    self.mut_rates[nuc1+nuc2] = []        

        for i in range(self.seq_length):
            # Determine the most frequent base at the position
            max_nuc = dict_max(self.base_counts[i])
        
            for nuc in self.base_counts[i].keys():
                if nuc != max_nuc and nuc != 'N':
                    self.mut_rates[max_nuc+nuc].append(self.norm_base_counts[i][nuc])
        
        # Normalize the mutation rates
        for mut in self.mut_rates.keys():
            self.mut_rates[mut]=(mean(self.mut_rates[mut]), stdev(self.mut_rates[mut]))
        
        self.timer['Calculate Specific Base Subsitution Rates'] = time()-start            


    def write_substitution_rates(self,output_file):
        with open(output_file,'w') as f:
            for mut in self.mut_rates.keys():
                f.write('{}->{}:\t{}\n'.format(mut[0],mut[1],self.mut_rates[mut]))
                

    def get_mutation_counts(self, offset=0, frame=1):
        self.ref_aa = Seq(self.ref_seq[frame-1:],generic_dna).translate()._data 
        self.aa_counts = dict()
        self.aa_muts = dict()
        
        for seq in self.seq_counts.keys():
            aa_seq = Seq(seq[frame-1:],generic_dna).translate()._data
            if aa_seq in self.aa_counts.keys():
                self.aa_counts[aa_seq]+=self.seq_counts[seq]
            else:
                self.aa_counts[aa_seq]=self.seq_counts[seq]    
                self.aa_muts[aa_seq]=get_mutations(self.ref_aa,aa_seq,offset)
        
        self.numbers['Unique AA Sequences']=len(self.aa_counts)

    def calc_codon_substitutions(self, frame=1):
        from math import floor
        
        self.codon_substitutions = dict()

        for seq in self.seq_counts.keys():
            mut_list = get_mutations(self.ref_seq, seq)
            
            codons = set([floor((int(i[1])-frame)/3) for i in mut_list])
            
            for i in codons:
                
                j = i*3+frame-1
                k = j+3
                if i>=0 and j <= len(self.ref_seq):                
                    new_codon = seq[j:k]
                    ref_codon = self.ref_seq[j:k]
                    
                    if (ref_codon, new_codon) in self.codon_substitutions.keys():
                        self.codon_substitutions[(ref_codon, new_codon)]+=add_counts(self.seq_counts[seq],True)
                    else:
                        self.codon_substitutions[(ref_codon, new_codon)]=add_counts(self.seq_counts[seq],True)
    
    def calc_mutation_freq(self):
        self.mut_freq=dict()
        #norm = len(self.aa_muts)
        
        norm = sum(self.aa_counts.values())
        
        for aa_seq in self.aa_muts.keys():
            mut_list = self.aa_muts[aa_seq]
            for mut in mut_list:
                if mut[0]+mut[1] in self.mut_freq.keys():
                    if mut[2] in self.mut_freq[mut[0]+mut[1]].keys():
                        self.mut_freq[mut[0]+mut[1]][mut[2]]+=self.aa_counts[aa_seq]/norm
                    else:
                        self.mut_freq[mut[0]+mut[1]][mut[2]]=self.aa_counts[aa_seq]/norm
                else:
                    self.mut_freq[mut[0]+mut[1]]=dict()
                    self.mut_freq[mut[0]+mut[1]][mut[2]]=self.aa_counts[aa_seq]/norm


    def write_mutation_freq(self, mut_freq_file):
        ofile = open(mut_freq_file,'w')
        AA_letters = "ARNDCQEGHILKMFPSTWYV"
        ofile.write('Position\t')
        ofile.write('\t'.join([i for i in AA_letters]))
        ofile.write('\n')
        for pos in self.mut_freq.keys():
            ofile.write(pos)
            ofile.write('\t')
            for AA in AA_letters:
                if AA in self.mut_freq[pos].keys():
                    ofile.write(str(self.mut_freq[pos][AA]))
                    ofile.write('\t')
                else:
                    ofile.write('0')
                    ofile.write('\t')
            ofile.write('\n')
        ofile.close()
    
    
    def write_mutation_ci(self, filename, keys=None):
        """write_mutation_ci
        Writes all the mutation coincidence rates to a file.
        
        Arguments:
            seqlib {SeqAnalysis}:
                A SeqAnalysis object for which the coincidence rates have been 
                calculated.
                
            filename {str}:
                The filepath to the output file.
                
            keys {list{str}=None}:
                A list of mutations that will appear in the output file.  If this
                argument is not given, all the coincidence rates in seqlib.ci will 
                be written to file.
        """
        # If the keys argument was not passed, use all the keys in seqlib.ci
        if not keys:
            keys = [i for i in self.ci.keys()]
            
        # Sort the keys, first by position, then by mutation
        keys = sorted(keys,key=lambda mut: (int(mut[1:-1]),mut[-1]))       
        
        # Initialize the variable storing the average coincidence
        averages = dict()
        for key in keys:
            averages[key]=0
            
        # Calculate the average coincidence for each key in keys
        # This calculates the average accross all mutations
        for key in keys:
            for key2 in keys:
                if key2 in self.ci[key].keys():
                    averages[key2]+=self.ci[key][key2]/self.ci[key][key]/len(self.ci.keys())
                
        # Open the output file
        ofile = open(filename,'w')
        
        # The first line is the list of mutations, separated by commas
        ofile.write(', , '+','.join(keys)+'\n')
        
        # The second line is the average coincidences for the mutations
        ofile.write(', , '+','.join([str(averages[i]) for i in keys])+'\n')
        
        # Iterate across the mutations in keys
        for mut1 in keys:
            # Write the mutation code
            ofile.write(mut1+', ')
            
            # Write the number of times the mutation appears in independent sequences
            ofile.write(str(self.ci[mut1][mut1])+', ')
            
            # Write all pairwise coincidence rates
            for mut2 in keys:
                try:
                    ofile.write(str(self.ci[mut1][mut2])+', ')
                except:
                    # If the two mutations never appeared together, the rate is zero
                    ofile.write('0, ')
                    
            ofile.write('\n')
        ofile.close() 
        
        
    def plot_mut_freq_map(self, mut_freq_file):
        """plot_mut_freq_map
        Plots a heatmap of the mutation frequency.  The x-axis is the position in 
        the sequencing, on the y-axis are all possible amino acid substitutions and
        the frequency of each substitution is color-coded.
        
        Arguments:
            mut_freq {dict{str:dict{str:float}}}:
                The mutation frequencies.  The keys are the positions and the values
                are dictionaries that store the frequency of each possible 
                subsitution.
                
            mut_freq_file {str}:
                A filepath where the figure will be saved.
        """
        # All the amino acids
        AAs = 'ARNDCQEGHILKMFPSTWYV'
        
        # Extract a list of positions from the mutation frequency dictionary
        positions = [i for i in self.mut_freq.keys()]
        
        # Sort the positions
        positions.sort(key=getKey)
        
        # Initialize the mutation array
        mut_freq_array = []
        
        # Add the mutation frequencies to the array
        for position in positions:
            line = []
            for AA in AAs:
                if AA in self.mut_freq[position].keys():
                    line.append(float(self.mut_freq[position][AA]))
                else:
                    line.append(0)
            mut_freq_array.append(line)
        
        # Convert to a numpy array        
        mut_freq_array = np.array(mut_freq_array)
        
        # Plot the heatmap
        plt.pcolor(mut_freq_array.transpose(), cmap='bwr', vmin=-0.00, vmax=0.05)
        
        # Save the figure
        plt.savefig(mut_freq_file, format='png', dpi=600)
    
    
    def calc_coincidence(self):
        """mutation_coincidence
        Calculates the coincidence rates for all pairs of mutations in the 
        sequencing.
        
        Arguments:
            mut_lists {list{list{tuple(str, str, str)}}}:
                A list where every entry is a list of mutations found in a 
                single clone.  The mutations are stored as tuples (A, B, C), 
                where A is the original residue, B is the position, and C is 
                the new AA identity.
        
        Returns
            mutation_ci {dict{str:dict{str:int}}}:
                The keys of mutation_ci are the individual mutations found in 
                the sequenced library.  The values are dictionaries where the 
                keys are the mutations are are found in the same clone as the 
                first mutation and the values are the number of clones the two 
                mutations appear in together.
        """
        mut_lists = list(self.aa_muts.values())
        
        # Convert the mutations from tuple form to str codes
        mutations = []
        for mut_list in mut_lists:
            mutations.append([i[0]+i[1]+i[2] for i in mut_list])
            
        # Initialize the output        
        self.ci = dict()
        
        # Interate through the clones
        for value in mutations:
            # Iterate through the mutations in the clone
            for i in range(len(value)):
                # Initialize the entry for mutation_ci[value[i]], if necessary
                if value[i] not in self.ci.keys():
                    self.ci[value[i]]=dict()
                
                # Iterate through the other mutations in the clone                
                for j in value[i:]:
                    # Initialize the entry for mutation_ci[j], if necessary
                    if j not in self.ci.keys():
                        self.ci[j]=dict()
                    
                    # Add a count for the mutation pair value[i]-j
                    if j not in self.ci[value[i]]:
                        self.ci[value[i]][j]=1
                    else:
                        self.ci[value[i]][j]+=1
                    
                    # Add a count for the mutation pair j-value[i]
                    if value[i] not in self.ci[j]:
                        self.ci[j][value[i]]=1
                    else:
                        # Do not add an extra count for self coincidence
                        if value[i] != j:
                            self.ci[j][value[i]]+=1
                            
    def calc_coincidence2(self, true_counts=False):
        """mutation_coincidence
        Calculates the coincidence rates for all pairs of mutations in the 
        sequencing.
        
        Arguments:
            mut_lists {list{list{tuple(str, str, str)}}}:
                A list where every entry is a list of mutations found in a 
                single clone.  The mutations are stored as tuples (A, B, C), 
                where A is the original residue, B is the position, and C is 
                the new AA identity.
        
        Returns
            mutation_ci {dict{str:dict{str:int}}}:
                The keys of mutation_ci are the individual mutations found in 
                the sequenced library.  The values are dictionaries where the 
                keys are the mutations are are found in the same clone as the 
                first mutation and the values are the number of clones the two 
                mutations appear in together.
        """
        # Initialize the output        
        self.ci = dict()
        
        all_muts=set()
        
        # Interate through the clones
        for aa_seq in self.aa_counts.keys():
            value = [i[0]+i[1]+i[2] for i in self.aa_muts[aa_seq]]
            
            all_muts.update(set(value))
            
            # Iterate through the mutations in the clone
            for i in range(len(value)):
                # Initialize the entry for mutation_ci[value[i]], if necessary
                if value[i] not in self.ci.keys():
                    self.ci[value[i]]=dict()
                
                # Iterate through the other mutations in the clone                
                for j in value[i:]:
                    # Initialize the entry for mutation_ci[j], if necessary
                    if j not in self.ci.keys():
                        self.ci[j]=dict()
                    
                    # Add a count for the mutation pair value[i]-j
                    if j not in self.ci[value[i]]:
                        self.ci[value[i]][j]=add_counts(self.aa_counts[aa_seq],true_counts)
                    else:
                        self.ci[value[i]][j]+=add_counts(self.aa_counts[aa_seq],true_counts)
                    
                    # Add a count for the mutation pair j-value[i]
                    if value[i] not in self.ci[j]:
                        self.ci[j][value[i]]=add_counts(self.aa_counts[aa_seq],true_counts)
                    else:
                        # Do not add an extra count for self coincidence
                        if value[i] != j:
                            self.ci[j][value[i]]+=add_counts(self.aa_counts[aa_seq],true_counts)


    def filter_ci(self,threshold):
        """filter_ci
        This function filters the mutations that are listed in the coincidence map.
        
        Arguments:
            seqlib {SeqAnalysis}: 
                A SeqAnalysis object with a coincidence map already calculated
        
            threshold {float}:
                A numeric value to threshold the coincidence frequency
                
        Returns:
            new_keys {list{str}}:
                A list of mutation codes that contain coincidence values that pass
                the filter.
        """ 
        # Initialize the new_keys
        new_keys = []
        
        # Look for mutations (ci keys) that have significant coincidence values 
        # with other mutations
        for key in self.ci.keys():
            for key2 in self.ci.keys():
                # Check if the two mutations ever appear together
                if key in self.ci[key2].keys():
                    # Look only at non-trivial coincidences
                    if key2 != key:
                        # Check if the fraction of coincidence passes the threshold
                        if self.ci[key2][key]/self.ci[key2][key2] > threshold:
                            # Check the number of times the mutation appears in the
                            # sequenced mutants.
                            if self.ci[key2][key2] > 1000:
                                # Add the mutation to the list of new keys
                                new_keys.append(key)
                                
                                # Once a mutation passes the filter, move on to the 
                                # next mutation
                                break
        
        return new_keys   


def add_counts(count,use_true_count):
    if use_true_count:
        return count
    else:
        return 1
    
def dict_max(my_dict):
    """dict_max
    Used to return the key in a dictionary with the maximum value
    
    Arguments:
        my_dict {dict{Any:Numeric}}:
            A dictionary.  The keys can be of any type and the values are an
            orderable type that can be compared to eachother (no mixing numeric
            and string types)
    """
    return max(my_dict, key=my_dict.get)


def save_data(data, save_file):
    """save_data
    Saves a variable to a file.
    
    Arguments:
        data {Any}:
            The variable that will be saved
        
        save_file {str}:
            A path to the output file
    """
    with open(save_file, 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
        

def load_data(save_file):
    """load_data
    Reads a variable from a file.
    
    Arguments:
        save_file {str}:
            A path to the output file
    
    Returns:
        data {Any}:
            The variable that was read from a file
    """
    with open(save_file, 'rb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        data = pickle.load(f)
    return data
    

def get_mismatch(seq,ref_seq):
    """get_mismatch
    Detects mismatches between two sequences
    
    Arguments:
        seq {str}:
            One sequence
        
        ref_seq {str}:
            Another sequence the same length as seq
    
    Returns:
        {list{bool}}:
            A list of booleans.  One element for each nucleotide in seq.  True
            indicates a mismatch.
    """
    assert len(seq) == len(ref_seq)
    return [nuc1 != nuc2 for nuc1, nuc2 in zip(seq, ref_seq)]
    
    
def count_mutations(seq,ref_seq):
    """count_mutations
    Counts the mismatches between two sequences
    
    Arguments:
        seq {str}:
            One sequence
        
        ref_seq {str}:
            Another sequence the same length as seq
            
    Returns:
        {int}:
            The number of mismatches between the two sequences
    """
    if len(seq) != len(ref_seq):
        return None
    else:
        return sum(get_mismatch(seq, ref_seq))
        

def find_frameshifts(alignment):
    """find_frameshifts
    Finds sequence insertions and deletions from an alignment
    
    Arguments:
        alignment {pairwise2.align.globalms entry}
            An alignment generated by pairwise2.align.globalms.
            
    Returns:
        insertions {list{int}}:
            A list of the positions with an insertion
            
        deletions {list{int}}
            A list of the positions with a deletion
    """
    # Extract the sequences from the alignment
    ref = alignment[0]
    seq = alignment[1]
    
    # Initialize i and j 
    i = 0
    j = 0
    position = 0
    insertions = []
    deletions = []
    
    # Track through the sequences until you reach one of the ends
    while (i < len(ref) and j < len(seq)):
        # Check for insertion
        if ref[i] == '-':
            insertions.append(position)
            # Scan until the end of the gap in the alignment
            while i < len(ref):
                try:
                    if ref[i+1] == '-':
                        i+=1
                    else:
                        break
                except:
                    i=i+1
        
        # Check for deletions
        if seq[j] == '-':
            deletions.append(position)
            # Scan until the end of the gap in the alignment
            while j < len(seq):
                try:
                    if seq[j+1] == '-':
                        j+=1
                    else:
                        break
                except:
                    j=j+1
        
        # Increment variables
        position+=1
        i+=1
        j+=1
    
    return insertions, deletions

                 
def get_mutations(ref,test,offset=0):
    i = 0
    mutations=[]
    for a,b in zip(ref,test):
        i+=1
        if a != b:
            mutations.append((a,str(offset+i),b))
        if b=='*':
            break
    return mutations
    
            
def getKey(item):
    """getKey
    This function is used to sort lists of mutation codes (i.e A52T) by 
    mutation position and then by the single letter code for the mutated AA.
    
    Arguments:
        item {str}:
            A string with a mutation code.
            
    Returns:
        {str}:
            A string encoding the position and mutated AA. For 'A52T' it will 
            return '000052T'
    """
    return '0'*(7-len(item))+item[1:]
            
            
def get_alignment(filename,template_name, ref_name):
    """get_alignment
    Reads an alignment file to map positions in the HIV AA sequence to positions
    in the HXB strain sequence.
    
    Arguments:
        filename {str}:
            A filepath to the alignment file.
            
        template_name {str}:
            The name of the strain that is mapped to the reference strain.  It 
            should be found in the alignment file.
            
        ref_name {str}:
            The name of the reference strain in the alignment file.
            
    Returns:
        algn {dict{int:int}}:
            A dictionary where the keys are positions in the template strain and
            the values are the aligned position in the reference strain.
    """
    # Open the alignment file
    ifile = open(filename)
    
    # Initialize the dictionary that holds the sequences
    seqs = dict()
    
    # Iterate through the alignment file
    for line in ifile:
        # Parse the lines
        words = line.split()
        if len(words)==3:
            if words[0] in seqs.keys():
                # Is the strain sequence has been started, extend the sequence
                seqs[words[0]]+=words[1]
            else:
                # The first word is the strain name, the second word is the sequence
                seqs[words[0]]=words[1]
                
    # i tracks the position in the template strain sequence
    i=0

    # j tracks the position in the reference strain sequence
    j=0
    
    # Initialize the alignment
    algn = dict()
    
    for a,b in zip(seqs[template_name],seqs[ref_name]):
        # If there is not a gap in the sequence, increment i and j
        if a != '-':
            i+=1
        if b != '-':
            j+=1
        
        # Update the alignment dictionary
        if i>0:
            algn[i]=j
            
    return algn
     
     
def get_mutation_count_distribution(seqlib):
    """get_mutation_count_distribution
    Get a list of the number mutations each unique sequence contains.
    
    Arguments:
        seqlib {SeqAnalysis}:
            A SeqAnalysis object where the amino acid mutatations have already
            been determined.
    
    Returns:
        counts {list{int}}:
            A list containing the number of mutations in each sequence
        
    """
    # The values of the dictionary seqlib.aa_muts are lists containing the 
    # mutation code for each mutation.  The length of the list equals the 
    # number of mutations.
    counts = [len(i) for i in seqlib.aa_muts.values()]
    return counts

    
def get_numbers(seqlib):
    """get_numbers
    This function prints out significant statistics about the sequencing run.
    
    Arguments:
        seqlib {SeqAnalysis}:
            A SeqAnalysis object that will be reported about.
    """
    print("Total Sequences: ", sum(seqlib.seq_counts.values()) + sum(seqlib.bad_seqs.values()))

    print("Unique Sequences: ", len(seqlib.seq_counts) + len(seqlib.bad_seqs))
    print("Sequences with multiple reads: ", len([i for i in seqlib.seq_counts.values() if i > 1]))
    print("Sequences with a single read: ", len([i for i in seqlib.seq_counts.values() if i == 1]))
    print("Sequences in this analysis: ", sum(seqlib.seq_counts.values()))
    print("AA Sequences (unique): ",len(seqlib.aa_counts))    
    print("AA Sequences (total): ",sum(seqlib.aa_counts.values()))
    print("Unique Sequences in this analysis: ", len(seqlib.seq_counts))

    
def print_numbers(seqlib):
    print('Total Reads:',seqlib.numbers['Total Reads'])
    print('Total Unique Reads:',seqlib.numbers['Total Unique Reads'])
    print("Total Sequences with multiple reads:",seqlib.numbers["Total Sequences with multiple reads"])
    print("Total Reads After Filter:",seqlib.numbers["Total Reads After Filter"])
    print('Total Unique Reads After Filter:',seqlib.numbers['Total Unique Reads After Filter'])
    print("Total Sequences with multiple reads After Filter:",seqlib.numbers["Total Sequences with multiple reads After Filter"])
    print('Unique AA Sequences:',seqlib.numbers['Unique AA Sequences'])
        
    
def process(files):
    """process
    The main analysis script.  This function reads in sequencing data, filters 
    the reads, calculates mutation frequencies, mutation coincidence rates, and
    outputs analysis files.
    
    Arguments:
        files {STRUCT}:
            A structure that contains the parameters needed to perform the 
            sequencing analysis.
    """
    # Create an instance of the SeqAnalysis class
    library = SeqAnalysis()

    # Read in sequences from fastq file
    library.get_sequence_counts(files.fastq, files.reverse)
    
    # Filter out bad sequences *Not strictly speaking necessary, this function is
    # implicitly called by calc_base_frequency
    library.filter_sequences(files.min_count)
    
    # Calculate base frequencies for each position in the sequence * Not strictly
    # necessary, this function is implicitly called by normalize_base_frequency
    library.calc_base_frequency()
    
    # Normalize by counts to get frequency.  This step will set the reference
    # sequence to be the consensus sequence
    library.normalize_base_frequency()
    
    # Filter the sequences again, now that a reference sequence has been 
    # specified
    library.filter_sequences(files.min_count)
    
    library.calc_base_frequency()
    
    # Normalize by counts to get frequency
    library.normalize_base_frequency()
    
    library.write_base_frequency_table(files.output)
    
    library.get_mutation_counts(files.offset, files.phase)
    library.calc_mutation_freq()
    library.write_mutation_freq(files.mut_freq)
    library.plot_mut_freq_map(files.mut_freq_map)
    library.calc_coincidence2(True)
#    library.write_mutation_ci(files.ci, library.filter_ci(0.10))
    library.write_mutation_ci(files.ci)
    library.calc_substitution_rates()
    library.calc_codon_substitutions(files.phase)
    
    return library

def write_substitution_rates(library,output_file):
    with open(output_file,'w') as f:
        for mut in library.mut_rates.keys():
            f.write('{}->{}:\t{}\t{}\n'.format(mut[0],mut[1],library.mut_rates[mut][0],library.mut_rates[mut][1]))
            
alignment = get_alignment('../Input/Alignment.txt','YU2','HXB2')     
    
files_1 = STRUCT(fastq='../Input/PostScreen/QHdSM-S2_M19S16_join.fastq',
                 output='../Output/QHdSM-S2_M19S16_join_3.csv',
                 mut_freq='../Output/QHdSM-S2_M19S16_mutation_AA.tab',
                 ci='../Output/QHdSM-S2_M19S16_mutation_ci_3.tab',
                 mut_freq_map='../Output/QHdSM-S2_M19S16_mutation_freq.png',
                 offset=540,
                 phase=2,
                 reverse=True,
                 min_count=2
                 )
                 
#library1 = process(files_1)
#print_numbers(library1)

files_2 = STRUCT(fastq='../Input/PostScreen/YU2dSMs3-5%_M19S15_join.fastq',
                 output='../Output/YU2dSMs3-5%_M19S15_join3_rev.csv',
                 mut_freq='../Output/YU2dSMs3_mutation_freq.tab',
                 ci='../Output/YU2dSMs3_mutation_ci_3.tab',
                 mut_freq_map='../Output/YU2dSMs3_mutation_AA.png',
                 offset=522,
                 phase=1,
                 reverse=True,
                 min_count=2         
                 )
                 
#library2 = process(files_2)             
#print_numbers(library2)
#mut_counts2 = library2.calc_mutation_dist()    

files_3 = STRUCT(fastq='../Input/Prescreen.fastq',
                 output='../Output/Prescreen3.csv',
                 mut_freq='../Output/Prescreen_mutation_freq_3.tab',
                 ci='../Output/Prescreen_mutation_ci_3.tab',
                 mut_freq_map='../Output/Prescreen_mutation_AA.png',
                 offset=540,
                 phase=2,
                 reverse=True,
                 min_count=None       
                 )
                 
#library3 = process(files_3)
#print_numbers(library3)

files_4 = STRUCT(fastq='../Input/M20-Hong/M20S01_S1_L001_join.fastq',
                 output='../Output/YU2-Prescreen3.csv',
                 mut_freq='../Output/YU2-Prescreen_mutation_freq_3.tab',
                 ci='../Output/YU2-Prescreen_mutation_ci_3.tab',
                 mut_freq_map='../Output/YU2-Prescreen_mutation_AA.png',
                 offset=522,
                 phase=1,
                 reverse=True,
                 min_count=None
                 )
                 
#library4 = process(files_4)

files_5 = STRUCT(fastq='../Input/M24S29_S29_join.fastq',
                 output='../Output/Unmutagenized.csv',
                 mut_freq='../Output/Unmutagenized.tab',
                 ci='../Output/Unmutagenized.tab',
                 mut_freq_map='../Output/Unmutagenized.png',
                 offset=522,
                 phase=1,
                 reverse=True,
                 min_count=None
                 )

library5 = process(files_5)
print_numbers(library5)

def analyze_codons(library):
    synonymous = 0
    nonsynonymous = 0
    for codons in library.codon_substitutions.keys():
        aa1 = Seq(codons[0],generic_dna).translate()._data 
        aa2 = Seq(codons[1],generic_dna).translate()._data 
        
        if aa1 == aa2:
            synonymous += library.codon_substitutions[codons]
        else:
            nonsynonymous += library.codon_substitutions[codons]
    
    return synonymous, nonsynonymous
    
        
#print(analyze_codons(library1))
#print(analyze_codons(library2))
#print(analyze_codons(library3))
#print(analyze_codons(library4))

#write_substitution_rates(library4, '../Output/YU2-Prescreen-Sub_Rates.tab')
#library2.write_substitution_rates('../Output/YU2-Postscreen_ExclSingletons-Sub_Rates.tab')
#write_substitution_rates(library2, '../Output/YU2-Postscreen-Sub_Rates.tab')

#mut_counts4 = library2.calc_mutation_dist()    
#get_numbers(library4)    

#count_dist = Counter([i for i in library2.seq_counts.values()])
#mut_count = dict()
#for key in ci.keys():
#    mut_count[key]=ci[key][key]

#AA_counts = library2.aa_counts
#ref_AA = library2.ref_aa
#AA_freqs = library2.mut_freq
#
#total = sum(AA_counts.values())
#
#freqs = []
#for i in range(len(ref_AA)):
#    freqs.append(AA_freqs[ref_AA[i]+str(i+523)])
#
#from pyseqlogo.pyseqlogo import draw_logo, setup_axis
#plt.rcParams['figure.dpi'] = 300
#fig, axarr = draw_logo(AA_freqs)
#fig.tight_layout()
#
#plt.rcParams['figure.dpi'] = 300
#fig, axarr = draw_logo(AA_freqs, data_type='bits', colorscheme='meme')
#fig.tight_layout()
    
#ofile = open('out.fasta','w')
#
#i = 0
#for AA_seq in AA_counts.keys():
##    ofile.write('>{}\n'.format(i))
#    ofile.write(AA_seq)
#    ofile.write('\n')
#    i+=1
#ofile.close()
#    
    
    