# -*- coding: utf-8 -*-
"""VCF_compare
This script compares the variant calling among multiple replicates of 
sequencing between parental and mutant strains to identify variant calls that
are present in the mutant strains, but not in the parental strains.  

This script also generates variant mapping to identify the genes with sequence
variants in the mutant strains.  It also performs GO term enrichment analysis.

This script assumes the presence of a vcf document for each sequencing run, 
databases that store genome features and sequencing read depths.

"""

import os
import matplotlib.pyplot as plt
import gffutils
import copy
import subprocess
import sqlite3
from math import floor
import re

from Bio import SeqIO
from Bio.Seq import Seq
from Bio import pairwise2
from Bio.Alphabet import generic_dna

class STRUCT:
    """
    This class approximates a C++ struct to hold multiple variables in a single
    object.
    
    """
    def __init__(self, **kwds):
        """__init__
        Creates the struct.  The variables contained in the STRUCT are defined
        by the keyword arguments passed to the constructor.
        
        Example:
            Struct_obj = STRUCT(
                            variable1=value1,
                            variable2=value2,
                            )
        """
        self.__dict__.update(kwds)
        
    def update(self, **kwds):
        self.__dict__.update(kwds)

MIN_READ_DEPTH = 50

class FeatureDB(gffutils.FeatureDB):
    """FeatureDB
    This class adds a few additional methods to the gffutils.FeatureDB class
    """
    def __init__(self, *args, **kwargs):
        """__init__
        All arguments and keyword arguments are passed on to the 
        gffutils.FeatureDB initiator
        """
        gffutils.FeatureDB.__init__(self, *args, **kwargs)
        
    def query_position(self, chrom, pos):
        """query_position
        This method identifies the genome features that cover a specific 
        genome position.
        
        Arguments:
            chrom {str}:
                A string identifying a chromosome (i.e. "1A","1B","2A","2B",...)
            pos {int}:
                An integer specifying the position in the chromosome
    
        Returns:
            features {list {str}}:
                A list of feature identifiers
        """
        # Convert the chomosome identifier into a sequence identifier that exists in the db
        seqid = 'Ca22chr{}_C_albicans_SC5314'.format(chrom)
    
        # Query the database
        cur = self.execute('SELECT id FROM features WHERE seqid="{}" AND start < {} AND end > {};'.format(seqid, pos, pos))
        ans = cur.fetchall()
    
        # Store the response in a list
        features = [tuple(item)[0] for item in ans]
    
        # Query for features on the reverse strand (start > end)
        cur = self.execute('SELECT id FROM features WHERE seqid="{}" AND start > {} AND end < {};'.format(seqid, pos, pos))
        ans = cur.fetchall()
    
        # Add the response to the list
        features.extend([tuple(item)[0] for item in ans])
    
        # Return the feature list
        return features
    
    
    def get_closest_feature(self, chrom, pos):
        """get_closest_feature
        This method identifies the nearest feature to a specified position in a
        chromosome
    
        Arguments:
            chrom {str}:
                A string identifying a chromosome {"1A","1B","2A","2B",...}
            pos {int}:
                An integer specifying the position in the chromosome
    
        Returns:
            distance {str}:
                The distance to the feature
            id {str}:
                The feature id
    
        """
        # Convert the chromosome identifier into a sequence id that is found in the DB
        seqid = 'Ca22chr{}_C_albicans_SC5314'.format(chrom)
        
        # Initialize the candidates list
        candidates = []        
    
        # Search for features whose start is closest to the position
        cur = self.execute('''
            SELECT abs({}-start) as AAA,id FROM features 
            WHERE
            seqid = "{}"
            ORDER BY AAA LIMIT 1
            '''.format(pos,seqid))
            
        candidates.append(cur.fetchone())
            
        # Search for features whose end is closest to the position     
        cur = self.execute('''
            SELECT abs({}-end) as AAA,id FROM features 
            WHERE
            seqid = "{}"
            ORDER BY AAA LIMIT 1
            '''.format(pos,seqid))
    
        candidates.append(cur.fetchone())
    
        if candidates[0] and candidates[1]: # Both queries returned features 
            if candidates[0][0] < candidates[1][0]: # Feature 1 is closer than Feature 2
                return candidates[0][1], candidates[0][0] # Return feature 1
            else: # Feature 2 is closer than feature 1
                return candidates[1][1], candidates[1][0] # Return feature 2
        elif not (candidates[0] or candidates[1]): # Neither query returned a feature 
            return # Return nothing
        elif candidates[0]: # The first query returned a feature
            return candidates[0][1], candidates[0][0] # Return feature 1
        else: #The second query returned a feature
            return candidates[1][1], candidates[1][0] # Return the second feature


    def get_size(self,name):
        """get_size
        A function for determining the size, in nucleotides, of a given genome
        feature.
        
        Arguments:
            name {str}:
                The genome feature name
        
        Returns:
            The length of the feature.  If the feature is not found in the 
            database, zero is returned
            
        """
        # Initialize the feature info variable
        info = None
        
        # Get information from the database.  If the name is not in the 
        # database, info remains None
        try:
            info = self[name]
        except:
            pass
        
        # If info is not None, return the length, otherwise return 0
        if info:
            return abs(info.end-info.start)
        else:
            return 0

        
    def map_variants(self,variant_list):
        """map_variants
        This method maps a list of variant sites to genome positions.
        
        Arguments:
            variant_list {list{tuple}}:
                A list of tuples, where each tuple has been generated by the 
                Tuple method of the Record class.
        
        Returns:
            variant_map {dict{str:int}}:
                A dictionary where the keys are genome feature names and the 
                associated values are the number of variants that mapped to 
                that feature.
                
            unmapped {list{tuple}}:
                A list of Record-generated tuples that do not map to any 
                annotated genome feature.        
        """
        variant_map = dict()
        unmapped = []
        
        for variant in variant_list:
            # Fetch a list of features that span the position of the variant
            mapping = self.query_position(variant[0], variant[1])
            
            # Sort the feature names by size of the feature
            mapping.sort(key=lambda name: self.get_size(name))
            
            if len(mapping) > 1:
                # Add the second longest feature (the longest feature is always
                # the entire chromosome)
                if mapping[-2][:11] in variant_map.keys():
                    variant_map[mapping[-2][:11]].append(variant)
                else:
                    variant_map[mapping[-2][:11]]=[variant]
            else:
                try:
                    mapping, dist = self.get_closest_feature(variant[0], variant[1])
                    if dist < 1000:
                        if mapping in variant_map.keys():
                            variant_map[mapping[:11]].append(variant)
                        else:
                            variant_map[mapping[:11]]=[variant]
                    else:
                        unmapped.append(variant)
                except:
                    unmapped.append(variant)
        
        return variant_map, unmapped
        
    
    def find_alias(self, alias):
        """find_alias
        Finds the systematic name for a ORF identifier.
        
        Arguments:
            alias {str}:
                The name to be queried.  It can be a gene name, like 'FKS1', or 
                an identifier from a genome assembly, like 'orf19.1116'.
        
        Returns:
            candidates {list{str}}:
                A list of ORFs systematic names where the alias appears in the 
                attributes string.
        """
        # Perform query on the FeatureDB
        cur = self.execute('SELECT id, attributes FROM features WHERE attributes LIKE \'%{}%\';'.format(alias))
        ans = cur.fetchall()

        # Initialize the candidates list
        candidates = set()
        
        # Process the results of the query
        for item in ans:
            # Sytematic names are always at least 9 characters long
            if len(item[0]) >= 9:
                # Filter out where the alias matched because it was part of a 
                # longer name (i.e. orf19.3932 shouldn't match orf19.3932.1)
                if re.search(alias+'(?!(\d|\.\d))',item[1]):
                    candidates.add(item[0][:9])
                    
        candidates = list(candidates)
        return candidates
        
    def load_genome(self, genome):
        if isinstance(genome, dict):
            self.genome=genome
        elif isinstance(genome, str):
            # Treat the genome variable like a filepath
            self.genome=read_fasta(genome)
            
    def get_sequence(self, feature_ID, variant_list=[]):
        
        counter=1
        out_seq = ''
        
        while True:
            try:
                feature=self[feature_ID+'-T-E'+str(counter)]
            except:
                if counter == 1:
                    return None
                else:
                    return out_seq

            chromosome=feature.seqid[7:9]
            
            if feature.end > feature.start:
                start = feature.start
                end = feature.end
            else:
                start = feature.end
                end = feature.start
                print('foo')
                
            ref_seq = self.genome[chromosome][start-1:end]
            
            sequence = ref_seq
     
            variant_list.sort(key=lambda variant: variant[1], reverse=True)
        
            for variant in variant_list:
                if variant[1]>start and variant[1]<=end and variant[3].find(',')<0:
                    sequence = sequence[:variant[1]-start]+variant[3]+sequence[variant[1]-start+len(variant[2]):]
        
            if feature.end < feature.start or feature_ID[8]=='C':
                sequence=rc(sequence)
            
            out_seq += sequence
            counter +=1
        


class Record:
    """Record
    This class is used to store the information in a single line in a vcf file.
    Each instance of this class corresponds to a single variant call.
    """
    def __init__(self, record_line=None):
        """init
        This constructs an instance of the Record class.  If a string (a line
        in the vcf file) is given, it is parsed to fill the properties of the
        Record class.
        """
        # Chromosome name
        self.chr = ''
        # Numeric position in the chromosome
        self.pos = None
        # The reference sequence
        self.ref = ''
        # The variant sequence
        self.alt = ''
        
        if record_line:
            self.parse_line(record_line)
    
    
    def parse_line(self, record_line):
        """parse_line
        Given a line from the vcf file, this function populates the information
        fields in the class.
        
        Arguments:
            record_line {str}:
                A string containing a line from the vcf file
        """
        # Split the line into words
        words = record_line.split()
        
        # Extract the chromosome name from the first 'word'.  This is specific
        # to the species
        self.chr = words[0][7:9]
        
        # The second word is the chromosome position of the variant 
        self.pos = int(words[1])

        # If the last word in the line only consists of 1's and 0's, treat the
        # line as coming from the output of a bcftools isec command, which 
        # provides less information
        if words[-1].strip('10')=='':
            # If the file is a result of bcftools isec operation,
            self.ref = words[2]
            self.alt = words[3]
        else:
            # If the file is a standard vcf file
            self.ref = words[3]
            self.alt = words[4]
            self.qual = float(words[5])
            self.info_text = words[7]
            
            # The info fields are stored in a dictionary
            self.info = dict()
            
            # Parse info field
            info_fields = words[7].split(';')
            for field in info_fields:
                values = field.split('=')
                if len(values)==2:
                    self.info[values[0]]=values[1]
            
            # Interpret the DP4 info field
            DP4 = self.info['DP4'].split(',')
            
            # Calculate the fraction of read supporting the variant call
            self.f_alt = (float(DP4[2])+float(DP4[3]))/(float(DP4[1])+float(DP4[2])+float(DP4[3])+float(DP4[0]))
            
            # Record the total number of reads at the variant position
            self.depth = int(self.info['DP'])
            
        
    def Tuple(self):
        """Tuple
        This method returns a tuple to uniquely identify a variant, which can 
        be used as dictionary keys.
        """
        return (self.chr, self.pos, self.ref, self.alt)

    
class VCF_File:
    """VCF_File
    A class for reading a vcf document and storing the variant information
    """
    def __init__(self, vcf_path=None):
        self.records = []
        
        if vcf_path:
            self.open_vcf(vcf_path)

    
    def open_vcf(self, vcf_path):
        """open_vcf
        Opens a VCF document and parses each line.  The variant calls records 
        are stored within the class.
        
        Arguments:
            vcf_path {str}:
                A filepath to the VCF file
        """
        # Open the VCF file
        vfile = open(vcf_path,'r')
        
        # Read the first line
        vline = vfile.readline()
        
        # Continue until the end of the document is reached
        while (vline):
            # If the line is not a comment, parse the variant call
            if vline.strip()[0]!='#':
                self.records.append(Record(vline))

            # Read the next line
            vline = vfile.readline()
        
        vfile.close()
    
                
def check_record(record):
    """check_record
    A function to filter vcf records.  Returns True is a record passes all the 
    requirements, False otherwise.
    """
    # Filter on mapping quality
    if record.qual < 60:
        return 
    
    # Filter on fraction of reads supporting the variant
#    if record.f_alt < 0.7:
#        return False
    
    # Filter on the read depth on the variant position
    if sum([int(i) for i in record.info['DP4'].split(',')]) < MIN_READ_DEPTH:
        return False
    
    # if it passes all the filters, return True
    return True


def import_strain(strain_name, strain_folder, replicate_list=None):
    """import_strain
    Compiles the variant information for a strain across all replicates
    
    Arguments:
        strain_name {str}:
            A string with the strain name
        
        strain_folder {str}:
            A string with a folder path to the folder containing the vcf files
        
        replicate_list {list[str]}:
            A list of strings to identify the variants.  The assumed vcf file
            name is strain_folder+strain_name+replicate_list[x]+'.vcf'
    
    Returns:
        strain {dict{str:dict{tuple:Record}}}:
            A dictionary with a key and value for each strain replicate.  The
            value for each replicate is another dictionary whose keys are 
            tuples generated from Record class objects and whose values are 
            corresponding Record objects.
        
        combined_variants {set}:
            A set of variants found in all the replicates for the strain.  The 
            items in the set are the tuples generated from the Record class.        
    """
    # Defines a default replicate list
    if not replicate_list:
        replicate_list = ['_1', '_2', '_3']
    
    # Initialize the strain dictionary
    strain = dict()
    
    # Initialize the combined_variants variable
    combined_variants = None
    
    # Iterate for each replicate
    for suffix in replicate_list:
        # Construct the replicate name
        isolate_name = strain_name+suffix
 
        # Initialize the dictionary entry for the replicate
        strain[isolate_name]=dict()
        
        # Read in the vcf file for the replicate
        isolate_vcf = VCF_File(os.path.join(strain_folder,isolate_name+'.vcf'))
        
        # Iterate over all the variants in the vcf file
        for record in isolate_vcf.records:
            # If the variant call passes all the filters, add it to the 
            # dictionary
            if check_record(record):
                strain[isolate_name][record.Tuple()]=record
                
        # Identify the variants that in common with all replicates
        if combined_variants:
            # Only keep the variants in combined variants that are also present
            # in the current replicate.
            combined_variants &= set(strain[isolate_name].keys())
        else:
            # combines_variants == None, so initialize combined variants with
            # variants from the first replicate
            combined_variants = set(strain[isolate_name].keys())

    # Return the output
    return strain, combined_variants


def write_feature_notes(feature_file, feature_list):
    """write_feature_notes
    Writes a file containing genome site names and associated notes.  The 
    fields are delimited by '!' characters.
    
    Arguments:
        feature_file {str}:
            A string containing the file path to the output file
        
        feature_list {list or set}:
            An iterable data structure containing strings with genome feature 
            names.
    """
    ofile = open(feature_file,'w')
    
    ofile.write('Site!Note!Length\n')

    for site in feature_list:
        feature = db[site]
        ofile.write(site[:12])
        ofile.write('!')
        if 'Note' in feature.attributes.keys():           
            ofile.write(', '.join(feature.attributes['Note']))        
        ofile.write('!')
        ofile.write(str(abs(feature.end-feature.start)))
        ofile.write('\n')
    ofile.close()
    

def generate_read_depth_files():
    """generate_read_depth_files
    A subroutine to generate read depth files for the ReadDepthDB class.  Only 
    needs to be run once.
    """
    names = ['JMC200-2-5', 'SMC60-2-5', 'JMC160-2-5']
    replicates = ["_1","_2","_3"]
    for strain in names:
        for replicate in replicates:
            isolate = strain+replicate
            bam_file = "/media/BigDisk/jzuber/Elena/Bam/"+isolate+".sorted.bam"
            depth_file = "/media/BigDisk/jzuber/Elena/"+isolate+".depth"
            with open(depth_file, "w") as outfile:
                exit_code=subprocess.call(["samtools", "depth", bam_file], stdout=outfile)


class ReadDepthDB:
    """ReadDepthDB
    A class for reading the depth files generated by the subroutine
    generate_read_depth_files(), importing the reads into a data base, and 
    providing an interface to access the read depth of any position.
    """
    def __init__(self, db_path=None):
        # Initialize the dictionary that converts a short chromosome name to 
        # the longer name used in the CGD files
        self.chromosomes = dict()
        
        # If a database path is given, open it
        if db_path:
            self.openDB(db_path)

        
    def openDB(self, db_path):
        """openDB
        Opens a sqlite database that contains the read depth information
        """
        # Open the database (or create it if it doesn't exist)
        self.db = sqlite3.connect(db_path)
        
        # Store the database cursor for executing queries
        self.cur = self.db.cursor()


    def AddTable(self, strain, chromosome, replicates):
        """AddTable
        Used to add a table to the database.  A table is generated for each 
        chromosome for each strain.
        
        Arguments:
            strain {str}:
                The strain name
            
            chromosome {str}:
                The short name for the chromosome
                
            replicates {list{str}}:
                A list of strings of replicate identifiers
        """
        #Check if table already exists
        self.cur.execute("select name from sqlite_master where type = 'table';")
        table_list = self.cur.fetchall()
        
        # The name of the new table
        table_name = strain+'_'+chromosome
        
        # If the table doesn't exist, add it
        if (table_name,) not in table_list:
            # Create table
            self.cur.execute('CREATE TABLE "{tn}" ({nc0} {ft} PRIMARY KEY)'\
                    .format(tn=table_name, nc0='Pos', ft='INTEGER'))
            
            # Add a column for the read depth of each replicate
            for replicate in replicates:
                self.cur.execute('ALTER TABLE "{tn}" ADD COLUMN "{nc1}" "{ct}"'\
                        .format(tn=table_name, nc1=strain+replicate, ct="INTEGER"))
            
            # Commit changes to the database
            self.db.commit()
            
                    
    def AddCount(self, strain, replicate, chromosome, position, count):
        """AddCount
        Adds a read count for a replicate of a strain
        
        Arguments:
            strain {str}:
                The strain name
            
            replicate {str}:
                A string of a replicate identifier
                
            chromosome {str}:
                The short name for the chromosome
                
            position {int}:
                The position of the chromosome
                
            count {int}
                The read depth at that position for that replicate
        """
        # The table to be updated
        table_name = strain+'_'+chromosome
        
        # The name of the position column
        c0 = 'Pos'
        
        # The name of the replicate column
        c1 = strain+replicate
        
        # Check if the row already exists
        self.cur.execute("SELECT * FROM '{tn}' WHERE Pos={pos};".format(tn=table_name, pos=position))
        
        if self.cur.fetchone():
            # If the row exists, update the replicate count
            self.cur.execute('UPDATE "{tn}" SET "{nc1}"={v1} WHERE {nc0} = {v0};'\
                .format(tn=table_name, nc0=c0, nc1=c1, v0=position, v1=count))
        else:
            # Else insert row
            self.cur.execute('INSERT OR REPLACE INTO "{tn}" ({nc0}, "{nc1}") VALUES ({v0}, {v1})'\
                .format(tn=table_name, nc0=c0, nc1=c1, v0=position, v1=count))
            

    def ReadDepths(self, strain, depth_folder, replicates=None):
        """ReadDepths
        Imports the read depth information for a strain
        
        Arguments:
            strain {str}:
                The strain name
             
            depth_folder {str}:
                A path to the folder the depth files are stored in
            
            replicates {list{str}=None}:
                A list of replicate identifiers.  If it is not provided, a 
                standard list of identifiers will be used.
        """
        # If a list of identifiers is not provided, use the default list
        if not replicates:
            replicates = ["_1","_2","_3"]
        
        # Import the read depth information for each replicate
        for replicate in replicates:
            # The replicate name
            isolate = strain+replicate
            print(isolate)

            # Construct the filepath to the depth file
            depth_file = os.path.join(depth_folder,isolate+".depth")
            
            with open(depth_file) as infile:
                # Read a line
                line = infile.readline()
                
                # The chromosome name of the last position to be read
                last_chr = ""
                
                while line:
                    words = line.split()
                    
                    # If there is a new chromosome
                    if words[0] != last_chr:
                        # Update the chromosome dictionary if necessary
                        if words[0] not in self.chromosomes.keys():
                            self.chromosomes[words[0]]=words[0][7:9]
                        
                        # Update the last chromosome
                        last_chr = words[0]
                        print(last_chr)
                        
                        # Add a table for the new chromosome
                        self.AddTable(strain,self.chromosomes[words[0]], replicates)
                    
                    # Add the read depth to the database
                    self.AddCount(strain, replicate, self.chromosomes[words[0]], 
                                  int(words[1]), int(words[2]))
                    
                    # Read the next line
                    line = infile.readline()
            
            # Commit changes to the database
            self.db.commit()
    
    
    def GetDepths(self, strain, chromosome, position):
        """GetDepths
        Gets the read depths of a position across replicates

        Arguments:
            strain {str}:
                The strain name
            
            chromosome {str}:
                The short name for the chromosome
                
            position {int}:
                The position of the chromosome

        Returns:
            response {tuple}:
                A read depth at the position for each replicate of the strain
        """
        # Figure out which table to query
        table_name = strain+'_'+chromosome
        
        # Query the position in the table
        self.cur.execute("SELECT * FROM '{tn}' WHERE Pos={v0}"\
            .format(tn=table_name, v0=position))
        
        # Fetch the response (the first column is the position)
        response = self.cur.fetchone()[1:]
        
        return response


    def GetChrReads(self, strain, chromosome):
        """GetChrReads
        Gets the total read depth (# reads mapped to the chromosome * average 
        read length) for a chromosome.  Returns a total for each replicate of 
        the strain.

        Arguments:
            strain {str}:
                The strain name
            
            chromosome {str}:
                The short name for the chromosome
                
        Returns:
            total_reads {list{int}}:
                The total read depth for the chromosome for each replicate of 
                the strain.        
        """
        # Identify the table name
        table_name = strain+'_'+chromosome
        
        # Pull all entries from the table
        self.cur.execute("SELECT * FROM '{tn}'"\
            .format(tn=table_name))
        responses = self.cur.fetchall()
        
        # Track the total for each replicate (column other than the first)
        total_reads = [0 for _ in range(len(responses[0])-1)]
        
        # Calculate the total read depth
        for response in responses:
            for i, read_count in enumerate(response[1:]):
                if read_count:
                    total_reads[i]+=read_count
        
        return total_reads


    def close(self):
        """close
        Closes the sqlite database
        """
        self.db.close()        
        
        
def read_fasta(fasta_file):
    """read_fasta

    This function parses a fasta file

    Args:
        fasta_file {str or list{str}}
            This argument can be either a string with a file path or a list 
            containing the lines of the fasta file, where each element is a 
            line in the file.

    Returns:
        seqs {dict{str:str}}
            A dictionary of nucleotide sequences.  The keys are sequence names
            and the values are the DNA sequences.
    """
    # Determine if fasta_file is a filename or the contents of the fasta file
    if isinstance(fasta_file, str):
        fp = open(fasta_file)
    elif isinstance(fasta_file, list):
        fp = fasta_file

    # Initialize name and sequence list
    name, seq = None, []
    
    # Initialize the output dictionary
    seqs = dict()
    
    # Iterate through each line in the fasta file
    for line in fp:
        # Remove flanking whitespace (including new lines)
        line = line.strip()
        
        # Check if the line contains header information
        if line.startswith(">"):
            # Check if this is the first header in the file
            if name:
                # Assemble the previous sequence and store it in the dictionary
                seqs[name]=''.join(seq)
            
            # Reinitialize the sequence line list
            seq = []
            
            # Determine the sequence name (this is specific to the c. albicans
            # genome)
            name = line[8:10]
        else:
            # Add the line to the sequence line list
            seq.append(line)
    
    # Assemble and add the final sequence            
    if name:
        seqs[name]=''.join(seq)
        
    # Return the output dictionary
    return seqs


def rc(seq):
    """rc
    Returns the reverse complement of a DNA sequence
    
    Arguments:
        seq {str}:
            A string containing a DNA sequence
            
    Returns:
        seq {str}:
            A string containing the reverse complement
    """
    # Define a dictionary to generate the complementary nucleotide
    bp = {'A':'T', 'C':'G', 'G':'C', 'T':'A', '-':'-', 'R':'Y', 'Y':'R', 'N':'N'}
    
    # Reverse the sequence
    seq = seq[::-1] 
    
    # Generate the complement
    seq = ''.join([bp[i] for i in seq])
    
    # Return the reverse complement
    return seq


def generate_alignments(fasta, mapping, gene, db):
    info = db[gene]
    chromosome=info.seqid[7:9]
    if info.end > info.start:
        start = info.start
        end = info.end
    else:
        start = info.end
        end = info.start
        
    ref_seq = fasta[chromosome][start-1:end]
    alignment=dict()
    for strain in mapping.keys():
        alignment[strain]=ref_seq
        if gene in mapping[strain]:
            alignment[strain]=generate_sequence(ref_seq, mapping[strain][gene], start)
#            variant_list = mapping[strain][gene]
#            variant_list.sort(key=lambda variant: variant[1], reverse=True)
#            for variant in variant_list:
#                alignment[strain] = alignment[strain][:variant[1]-start]+variant[3]+alignment[strain][variant[1]-start+len[variant[2]]:]
    
        if info.end < info.start:
            alignment[strain]=(rc(alignment[strain][0]), rc(alignment[strain])[1])
    
    i = dict()
    output_alignment = dict()
    for strain in alignment.keys():
        output_alignment[strain]=''
        i[strain]=0

    to_break=False

    while True:
        # Check for gaps in reference sequences in alignments
        add_gap = False
        for strain in alignment.keys():
            if alignment[strain][0][i[strain]] == '-':
                add_gap = True
                break
            
        if add_gap:
            for strain in alignment.keys():
                if alignment[strain][0][i[strain]] == '-':
                    output_alignment[strain]+=alignment[strain][1][i[strain]]
                    i[strain]+=1
                else:
                    output_alignment[strain]+='-'
        else:
            for strain in alignment.keys():
                output_alignment[strain]+=alignment[strain][1][i[strain]]
                i[strain]+=1
        
        
        for strain in alignment.keys():
            if i[strain]==len(alignment[strain][0]):
                to_break=True
        
        if to_break:
            break
            
    return output_alignment
        
        
def generate_sequence(ref_seq, variant_list, start):
    seq = ref_seq
    variant_list.sort(key=lambda variant: variant[1], reverse=True)

#    for variant in variant_list:
#        if len(variant[3])<=len(variant[2]):
#            seq = seq[:variant[1]-start]+variant[3]+'-'*(len(variant[2])-len(variant[3]))+seq[variant[1]-start+len[variant[2]]:]
#        else:
#            seq = seq[:variant[1]-start]+variant[3]+seq[variant[1]-start+len[variant[2]]:]
#            ref_seq = ref_seq[:variant[1]-start+len[variant[2]]]+'-'*(len(variant[3])-len(variant[2]))+seq[variant[1]-start+len[variant[2]]:]
#    return ref_seq, seq
    
    for variant in variant_list:
        seq = seq[:variant[1]-start]+variant[3]+seq[variant[1]-start+len[variant[2]]:]

    return seq
    

def count_bins(data, lower, upper, bins):
    """count_bins
    Generate counts for histogram output
    
    Arguments:
        data {iterable numeric type}:
            A iterable object that produces numeric types.  Can be a list, set,
            or other custom type.
        
        lower {numeric}:
            A numeric type that specifies the lower bounds for the bins
            
        upper {numeric}:
            A numeric type that specifies the upper bounds for the bins
            
        bins {int}:
            A integer specifying the number of bins to put the counts in
            
    Returns:
        counts {list{int}}:
            A list of counts for the bins
            
        bins {list{float}}:
            A list of the center point of each bin            
    """
    bin_width = (upper - lower)/bins

    bin_points = []
    counts = []
    span = upper - lower
    
    # Find the boundary for each bin and initialize the counts list
    for i in range(bins):
        bin_points.append(lower+ i*bin_width)
        counts.append(0)
    # Add the last bin boundary
    bin_points.append(lower+ bins*bin_width)
    
    # Distribute the counts to the bins
    for item in data:
        # Calculate the bin number
        index = floor((item-lower)/span*bins)
        
        # Add a count to the bin
        if item == upper:
            counts[index-1] += 1
        else:
            counts[index] += 1
    
    # Find the center of each bin
    bins = [(bin_points[i]+bin_points[i+1])/2 for i in range(len(bin_points)-1)]    
    
    return counts, bins
    

def plot_read_fraction_dist(rf_data, strain, odir):
    """plot_read_fraction_dist
    Plot the distribution of read fractions supporting each variant
    
    Arguments:
        rf_data {list{float}}:
            A list of read fractions supporting a variant.  Each item in the
            list represents a different variant.
        
        strain {str}:
            A string specifying the strain name.  This is used to annotate the 
            figure.
        
        odir {str}:
            A string specifying a folder path where the figure will be saved.
    """
    plt.figure(figsize=(6, 3), dpi=600)
    counts, bins = count_bins([i for i in rf_data.values()], 0, 1, 50)
    
    plt.bar(bins, counts, width=0.7/50, color='grey', align='center')
    plt.xlabel('Fraction of reads supporting variant')
    plt.ylabel('Count')
    plt.title('Read Fraction Distribution ({})'.format(strain))
    plt.gca().set_xlim([0,1])
    plt.savefig(os.path.join(odir, strain+'_read_fraction_dist.png'), format='png', dpi=600)
    

def plot_read_fraction_diff_dist(rf_data, strain, odir):
    """plot_read_fraction_diff_dist
    Plot the distribution of the dfference in read fractions supporting each 
    variant in the mutant and parent strain.
    
    Arguments:
        rf_data {list{float}}:
            A list of read fraction differences supporting a variant.  Each item 
            in the list represents a different variant.
        
        strain {str}:
            A string specifying the strain name.  This is used to annotate the 
            figure.
        
        odir {str}:
            A string specifying a folder path where the figure will be saved.
    """
    plt.figure(figsize=(6, 3), dpi=600)
    counts, bins = count_bins([i for i in rf_data.values()], -1, 1, 100)
    plt.bar(bins, counts, width=0.7/50, color='grey', align='center')
    plt.xlabel('Read Fraction Difference')
    plt.ylabel('Count')
    plt.title('Read Fraction Difference Distribution ({})'.format(strain))
    plt.gca().set_xlim([-1,1])
    plt.savefig(os.path.join(odir, strain+'_read_fraction_diff_dist.png'), format='png', dpi=600)        
    

def gen_association_file(in_file, out_file):
    """
    Converts a cgd file from the candida genome database to an association file read
    by goatools.
    """    
    with open(in_file) as ifile:
        with open(out_file,'w') as ofile:
            line = ifile.readline()
            while line:
                if line.strip()[0]!='!':
                    words=line.split('\t')
                    for name in words[10].split('|'):
                        if name != '':
                            ofile.write('{}\t{}\n'.format(name,words[4]))
                line=ifile.readline()
                

def gen_population_file(in_file, out_file):
    """
    Converts a cgd file from the candida genome database to a population file read
    by goatools.  The output needs to be manually curated.
    """    
    with open(in_file) as ifile:
        with open(out_file,'w') as ofile:
            line = ifile.readline()
            while line:
                # If the first character is a '!', the line is a comment
                if line.strip()[0]!='!':
                    # Write the gene name to the output file
                    words=line.split('\t')
                    name = words[10].split('|')[0]
                    if name != '':
                        ofile.write('{}\n'.format(name))
                
                # Read the next line
                line=ifile.readline()                        


def print_read_coverage(strains, DepthDB, db, chr_string):
    """print_read_coverage
    Prints total read coverage and chromosome length information that can be
    used to calculate average read coverage for each chromosome.
    
    Arguments:
        strains {list{str}}:
            A list of strain names.  The function will print out statistics for
            each strain in the list.  Each strain needs to have read depth 
            information in the DepthDB.
        
        DepthDB {ReadDepthDB}:
            A ReadDepthDB object that contains read depth information for each
            chromosome for each strain.
            
        db {FeatureDB}:
            A gffutils feature database generated from a gff document.
            
        chr_string:
            A string that will be formatted to generate chromosome feature 
            names as found in the gff document.
    """
    # A list of chromosome names to consider
    chromosomes = ["1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B", "5A", "5B", 
                   "6A", "6B", "7A", "7B", "RA", "RB"]
    
    # Initialize a dictionary that will store the length of each chromosome
    chr_lengths = dict()
    
    # Get the lengths of each chromosome
    for chromosome in chromosomes:
        # Get the feature object from the FeatureDB
        chr_lengths[chromosome]=db[chr_string.format(chromosome)].end
    
    # Print out information for each strain
    for strain in strains:
        print(strain)
        
        # Print column names
        print('\tChr\tLength\tCounts')
        
        # Print information for each chromosome
        for chromosome in chromosomes:
            # Get the total read depth for the chromosome (not equivalent to read count)
            chr_count = DepthDB.GetChrReads(strain,chromosome)
            
            print('\t{}:\t{}'.format(chromosome,chr_lengths[chromosome]),end='')
            for count in chr_count:
                print('\t{}'.format(count),end='')
            
            # Start a new line
            print()
        
        # Add a gap between strains
        print('\n')                    


def write_map_file(mapped,db,map_file):
    """write_map_file
    Writes a file with the number of variants mapped to each gene with variants
    across multiple strains.
    
    Arguments:
        mapped {dict{str:dict{str:int}}}:
            A dictionary with a key and value for each strain.  The value for 
            the strain is another dictionary whose keys are ORF names and the 
            values are the number of variants mapped to those ORFs.
        
        db {FeatureDB}:
            A genome feature database. Can be used to annotate the ORF entry.
        
        map_file {str}:
            A path to the output file.
    """
    names = [i for i in mapped.keys()]
    
    sites = set()
    for name in names:
        sites |= set([i for i in mapped[name].keys()])
    
    sites = list(sites)    
    
    ofile = open(map_file,'w')    
    ofile.write('Site\tChromosome\t')
    ofile.write('\t'.join(names))
    ofile.write('\tAliases\n')
    for site in sites:
        try:
            feature = db[site]
            ofile.write(site)
            ofile.write('\t')
            ofile.write(feature.seqid[7:9])
            ofile.write('\t')
            for name in names:
                if site in mapped[name].keys():
                    ofile.write(str(len(mapped[name][site])))
                    ofile.write('\t')
                else:
                    ofile.write('0\t')
            ofile.write('\n')
        except:
            print(site)
    ofile.close()   
    
def get_mutations(ref,test,offset=0):
    i = 0
    mutations=[]
    for a,b in zip(ref,test):
        i+=1
        if a != b:
            mutations.append((a,str(offset+i),b))
        if b=='*':
            break
    return mutations
    
# Open the features database
db = FeatureDB('C_albicans_features.db', keep_order=True)

ref_seq = read_fasta(os.path.join('../Input/Elena','C_albicans_SC5314_A22_current_chromosomes.fasta'))
#ref_seq21 = read_fasta('/media/BigDisk/jzuber/Elena/C_albicans_SC5314_A21_current_chromosomes.fasta')
db.load_genome(ref_seq)


#*****************************************************************************#
#  Read vcf files                                                             #    
#*****************************************************************************#

# a list of strain names
names = ['JMC200-2-5', 'SMC60-2-5', 'JMC160-2-5', 'JMC200-3-4']
ref_names = [ 'JRCT1', 'SC5314', 'JRCT1', 'JRCT1']

num_replicates = {'JMC200-2-5':3, 
                  'SMC60-2-5':3, 
                  'JMC160-2-5':3,
                  'JMC200-3-4':2,
                  'JRCT1':3, 
                  'SC5314':3}
                
references = ['SC5314', 'JRCT1']

combined_ref = dict()

all_names = [i for i in references]
all_names.extend(names)

# The variants are stored in a dictionary of dictionaries.  The keys for 
# isolates are strain replicate names.  The dictionary for each replicate  uses
# keys that are tuples from the record class and the values are Record class
# instances. isolates stores all variant records in all vcf files.
isolates=dict()

#  Identify variants in parent strain
for ref in references:
    isolates[ref], combined_ref[ref]=import_strain(ref, '../Input/Elena')

# Variants stores the variants in each strain that appear in all three 
# replicates.  The keys are strain names and the values are lists of tuples
# generated from variant records. 
variants = dict()

# Iterate for each mutant strain
for name, ref_name in zip(names, ref_names):
    replicate_list = ['_'+str(i+1) for i in range(num_replicates[name])]
    # Import information for all strain replicates
    isolates[name], combined_variants=import_strain(name,'../Input/Elena', replicate_list)
    
    # Find variants in the mutant strain that are not in the parent strain
    variants[name]=list(combined_variants.difference(combined_ref[ref_name]))

#*****************************************************************************#
#  Analyze read fractions for each variant                                    #    
#*****************************************************************************#
DepthDB = ReadDepthDB('../Input/Elena/DepthDB.sqlite')
#DepthDB.ReadDepths('JMC200-3-4','/media/BigDisk/jzuber/Elena',['_1','_2'])

# read_fractions contains the average fraction of reads supporting a variant
# call for each strain, averaged across all isolates
read_fractions = dict()

# Calculate the read fractions for the parent strains
for name in all_names:
    # Initialize the dictionary storing the reac fractions for the strain
    read_fractions[name]=dict()
    
    replicate_list = ['_'+str(i+1) for i in range(num_replicates[name])]
    
    for suffix in replicate_list:
        # Construct the isolate name
        isolate_name = name+suffix
        
        # Iterate through each variant record in the replicate
        for variant in isolates[name][isolate_name].keys():
            read_counts = DepthDB.GetDepths(name, variant[0], variant[1])
            replicate_count = len([i for i in read_counts if i])
            
            if variant in read_fractions[name].keys():
                # If the variant is present in the ref_variants_f, add to the average
                read_fractions[name][variant]+=isolates[name][isolate_name][variant].f_alt/replicate_count
            else:
                # Otherwise, create a new dictionary entry
                read_fractions[name][variant]=isolates[name][isolate_name][variant].f_alt/replicate_count

#    plot_read_fraction_dist(read_fractions[name],name, '/media/BigDisk/jzuber/Elena')

# read_fractions_diff holds the difference between the fraction of reads 
# supporting a variant in the mutant and the parent strain
read_fractions_diff = dict()

# Iterate for each mutant strain
for name, ref_name in zip(names, ref_names):
    # Copy the read fractions for the mutant
    read_fractions_diff[name] = copy.deepcopy(read_fractions[name])
    
    # Subtract the read fractions for the parent strain
    for variant in read_fractions[ref_name].keys():
        # Subtract the parent read fraction (creating a new entry if necessary)
        if variant in read_fractions_diff[name].keys():
            read_fractions_diff[name][variant]-=read_fractions[ref_name][variant]
        else:
            read_fractions_diff[name][variant]=-1*read_fractions[ref_name][variant]
#
#    plot_read_fraction_diff_dist(read_fractions_diff[name], name, '/media/BigDisk/jzuber/Elena')


#*****************************************************************************#
#  Filter the read fraction difference for the population of variants that    #
#  are present in all replicates of each strain.                              #    
#*****************************************************************************#
read_fractions_diff_2 = dict()
for name in names:
    read_fractions_diff_2[name] = dict()
    
    for variant in variants[name]:
        read_fractions_diff_2[name][variant]=read_fractions_diff[name][variant]
    
#    plot_read_fraction_diff_dist(read_fractions_diff_2[name], name+'-old_method', '/media/BigDisk/jzuber/Elena')


#*****************************************************************************#
#  Filter variants based on read fraction difference                          #    
#*****************************************************************************#
# Set a threshold
threshold = 0.67

# Initialize the output
interesting_variants = dict()

# Iterate for each mutant strain
for strain in names:
    # Initialize the list of interesting variants
    interesting_variants[strain]=[]
    
    # Iterate through each key:item pair in the dictionary
    for variant, value in read_fractions_diff[strain].items():
        # Add the variant to the list of interesting variants if the read
        # fraction passes the threshold
        if value >= threshold:
            interesting_variants[strain].append((variant,value))


#*****************************************************************************#
#  Map the variants to genes                                                  #    
#*****************************************************************************#
# Initialize the dictionaries that store the mapped and unmapped variants
unmapped = dict()
mapped = dict()

# Iterate through each mutant strain
for clone in variants.keys():
    mapped[clone], unmapped[clone] = db.map_variants([i[0] for i in interesting_variants[clone]])


interesting_genes = set()
for clone in mapped.keys():
    interesting_genes |= set(mapped[clone].keys())

interesting_genes = list(interesting_genes)    


#*****************************************************************************#
#  Map the variants in the parental strains                                   #    
#*****************************************************************************#

# Set a threshold
ref_threshold = 0.67

# Initialize the output
interesting_ref_variants = dict()

# Iterate for each mutant strain
for strain in ref_names:
    # Initialize the list of interesting variants
    interesting_ref_variants[strain]=[]
    
    # Iterate through each key:item pair in the dictionary
    for variant, value in read_fractions[strain].items():
        # Add the variant to the list of interesting variants if the read
        # fraction passes the threshold
        if value >= ref_threshold:
            interesting_ref_variants[strain].append((variant,value))
            
# Initialize the dictionaries that store the mapped and unmapped variants
ref_unmapped = dict()
ref_mapped = dict()

# Iterate through each mutant strain
for clone in ref_names:
    ref_mapped[clone], ref_unmapped[clone] = db.map_variants([i[0] for i in interesting_ref_variants[clone]])
    
#*****************************************************************************#
#  Output the number of                                                       #    
#*****************************************************************************#
names = ['JMC200-2-5', 'SMC60-2-5', 'JMC160-2-5', 'JMC200-3-4']
        
write_map_file(mapped,db,'Variant_Sites5.tab')                

#*****************************************************************************#
# Get variant mapping for SC5314                                              #
#*****************************************************************************#
ref_map=dict()
for ref in references:
    ref_map[ref]=dict()
    ref_map[ref], foo = db.map_variants(combined_ref[ref])

write_map_file(ref_map,db,'ReferenceSites.tab')            


#*****************************************************************************#
# Map variants to genes                                                       #
#*****************************************************************************#
unmapped2 = dict()
mapped2 = dict()
for clone in variants.keys():
    mapped2[clone], unmapped2[clone] = db.map_variants(variants[clone])


#*****************************************************************************#
# Try and find common unmapped regions                                        #
#*****************************************************************************#
common_unmapped = dict()
threshold = 5000
for clone in unmapped.keys():
    common_unmapped[clone]=[]
#    mapped[clone]=dict()
    for variant in unmapped[clone]:
        select = dict()
        for clone2 in unmapped.keys():
            if clone2 != clone:
                select[clone2] = [ (i[0], i[1]) for i in unmapped[clone2] if i[0]==variant[0] and abs(i[1]-variant[1]) < threshold] != []
        if all(i for i in select.values()):
            common_unmapped[clone].append(variant)
            

#*****************************************************************************#
# Perform GO enrichment analysis                                              #
#*****************************************************************************#
from goatools.obo_parser import GODag

obodag = GODag('../Input/Elena/gene_ontology.1_2.obo')

from goatools.go_enrichment import GOEnrichmentStudy

# Import the associations between ORF ID and GO terms
go_associations = dict()
with open('../Input/Elena/association') as f:
    for line in f:
        words = line.split()
        if words[0] in go_associations.keys():
            go_associations[words[0]]|={words[1]}
        else:
            go_associations[words[0]]={words[1]}

# Import the background set of genes
population=[]
with open('../Input/Elena/population') as f:
    for line in f:
        words = line.split()
        population.append(words[0])

population = set([i for i in population if i[-1]=='A'])
        
# Import the set of genes that the enrichment analysis is performed on.        
study_genes=[]
with open('../Input/Elena/study_genes5.txt') as f:
    for line in f:
        words = line.split()
        study_genes.append(words[0])

study_genes = set(study_genes)

# Set up the analysis
goeaobj = GOEnrichmentStudy(
        population, # List of protein-coding genes
        go_associations, # geneid/GO associations
        obodag, # Ontologies
        propagate_counts = False,
        alpha = 0.05, # default significance cut-off
        methods = ['fdr_bh']) # defult multipletest correction method        

# Execute the analysis
goea_results_all = goeaobj.run_study(study_genes)
goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < 0.2]

# Extract the p-values and sort them
p_vals = [r.p_fdr_bh for r in goea_results_all]
p_vals.sort()

#*****************************************************************************#
#  Calculate the average read depth of each chromosome                        #
#*****************************************************************************#
strains = ['SC5314', 'JMC200-2-5', 'SMC60-2-5', 'JMC160-2-5', 'JRCT1', 'JMC200-3-4']
chr_string = 'Ca22chr{}_C_albicans_SC5314'
#print_read_coverage(strains,DepthDB,db,chr_string)


#*****************************************************************************#
#  Map downregulated genes (from RNAseq analysis)                             #
#*****************************************************************************#
with open('../Input/Elena/DownGenes') as f:
    with open('../Output/DownGenesID','w') as fo:
        for line in f:
            line = line.strip()
            ids = db.find_alias(line)
            fo.write(line)
            fo.write('\t')
            fo.write(', '.join(ids))
            fo.write('\n')


genes_aa = dict()

mutations_aa = dict()
for strain in mapped.keys():
    mutations_aa[strain]=dict()
    
            
for gene in interesting_genes:
    nuc_seq = db.get_sequence(gene)
    if nuc_seq:
        for i in range(len(names)):
            strain=names[i]
            parental = ref_names[i]
            if gene in ref_mapped[parental].keys():
                nuc_seq = db.get_sequence(gene, ref_mapped[parental][gene])
            ref_aa = Seq(nuc_seq,generic_dna).translate()._data
            genes_aa[gene]=ref_aa
            if gene in mapped[strain].keys():
                mut_seq = db.get_sequence(gene,[_ for _ in mapped[strain][gene] if read_fractions_diff[strain][_]>0.9])
                mut_aa = Seq(mut_seq,generic_dna).translate()._data
                mut_list=get_mutations(ref_aa, mut_aa)
                mutations_aa[strain][gene]=[]
                for mut in mut_list:
                    mutations_aa[strain][gene].append(''.join(mut))
    else:
        print(gene, ' is not an ORF')
            
        
with open('Variant_Scores3','w') as f:
    for strain in interesting_variants.keys():
        f.write(strain)
        f.write('\n')
        f.write('\t'.join(['Gene','Gene Start','Gene End','Introns?','Chromosome','Position','Reference','Alternate','Score']))
        f.write('\n')        
        for entry in interesting_variants[strain]:
            variant = entry[0]
            # Fetch a list of features that span the position of the variant
            mapping = db.query_position(variant[0], variant[1])
            
            # Sort the feature names by size of the feature
            mapping.sort(key=lambda name: db.get_size(name))
            
            # Write the mapped ORF for the variant
            if len(mapping) > 1:
                f.write(mapping[-2][:11])
                f.write('\t')
                f.write(str(db[mapping[-2][:11]].start))
                f.write('\t')
                f.write(str(db[mapping[-2][:11]].end))
                f.write('\t')
                try:
                    introns = db[mapping[-2][:11]+'-T-I1']
                    f.write('True')
#                    f.write('\t')
                except:
                    f.write('False')
#                    f.write('\t')
            else:
                try:
                    mapping, dist = db.get_closest_feature(variant[0], variant[1])
                    if dist < 1000:
                        f.write('Near '+mapping[:11])
                        f.write('\t')
                        f.write(str(db[mapping[:11]].start))
                        f.write('\t')
                        f.write(str(db[mapping[:11]].end))
                        f.write('\t')
                        try:
                            introns = db[mapping[-2][:11]+'-T-I1']
                            f.write('True')
#                            f.write('\t')
                        except:
                            f.write('False')
        #                    f.write('\t')

                    else:
                        f.write('Unmapped')
                        f.write('\t')
                        f.write('\t')
                        f.write('\t')
#                        f.write('\t')

                except:
                    f.write('Unmapped')
                    f.write('\t')
                    f.write('\t')
                    f.write('\t')
#                    f.write('\t')

            f.write('\t')
            
            # Write the position and sequence information for the variant
            f.write('\t'.join([str(i) for i in variant]))
            f.write('\t')
            f.write(str(entry[1]))
            
            # Write the fractional read support for a variant
            for replicate in isolates[strain].keys():
                f.write('\t')
                if variant in isolates[strain][replicate].keys():
                    f.write(str(isolates[strain][replicate][variant].f_alt))
                else:
                    f.write('0')
            
            # Write the read depth associated with the variant call
            for replicate in isolates[strain].keys():
                f.write('\t')
                if variant in isolates[strain][replicate].keys():
                    f.write(str(sum([int(i) for i in isolates[strain][replicate][variant].info['DP4'].split(',')])))
                else:
                    f.write('0')
            f.write('\t')                    

            if isinstance(mapping, str):
                f.write(mapping)
            else:
                f.write(','.join(mapping))
            f.write('\t')
            
            try:
                f.write(', '.join(mutations_aa[strain][mapping[-2][:11]]))
            except:
                pass
            f.write('\n')
        f.write('\n')
            
# Generate Sequence Alignments    

